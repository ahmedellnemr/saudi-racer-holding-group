<?php
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//--------------- Login   ---------------------------
$route['(ar|en)/auth'] = 'Login/checkLogin';
$route['auth']         = 'Login/checkLogin';
$route['(ar|en)/logout'] = 'Login/logout';
$route['logout']         = 'Login/logout';

//--------------- Dashboard   ----------------------
$route['(ar|en)/Dashboard'] = 'Dashboard';
$route['Dashboard'] = 'Dashboard';

//--------------- main-setting   ----------------------
$route['(ar|en)/main-setting'] = 'MainData/add';
$route['main-setting'] = 'MainData/add';
$route['(ar|en)/main-setting/(:any)'] = 'MainData/$1';
$route['main-setting/(:any)'] = 'MainData/$1';
$route['(ar|en)/main-setting/(:any)/(:any)']= 'MainData/$1/$2';
$route['main-setting/(:any)/(:any)']= 'MainData/$1/$2';

//--------------- News   ----------------------
$route['(ar|en)/news'] = 'News';
$route['news'] = 'News';
$route['(ar|en)/news/(:any)'] = 'News/$1';
$route['news/(:any)'] = 'News/$1';
$route['(ar|en)/news/(:any)/(:any)']= 'News/$1/$2';
$route['news/(:any)/(:any)']= 'News/$1/$2';

//--------------- Partner   ----------------------
$route['(ar|en)/partner'] = 'Partner';
$route['partner'] = 'Partner';
$route['(ar|en)/partner/(:any)'] = 'Partner/$1';
$route['partner/(:any)'] = 'Partner/$1';
$route['(ar|en)/partner/(:any)/(:any)']= 'Partner/$1/$2';
$route['partner/(:any)/(:any)']= 'Partner/$1/$2';

//--------------- Partner   ----------------------
$route['(ar|en)/branch'] = 'Branch';
$route['branch'] = 'Branch';
$route['(ar|en)/branch/(:any)'] = 'Branch/$1';
$route['branch/(:any)'] = 'Branch/$1';
$route['(ar|en)/branch/(:any)/(:any)']= 'Branch/$1/$2';
$route['branch/(:any)/(:any)']= 'Branch/$1/$2';

//--------------- TeamWork   ----------------------
$route['(ar|en)/team-work'] = 'TeamWork';
$route['team-work'] = 'TeamWork';
$route['(ar|en)/team-work/(:any)'] = 'TeamWork/$1';
$route['team-work/(:any)'] = 'TeamWork/$1';
$route['(ar|en)/team-work/(:any)/(:any)']= 'TeamWork/$1/$2';
$route['team-work/(:any)/(:any)']= 'TeamWork/$1/$2';
//--------------- Slider   ----------------------
$route['(ar|en)/slider'] = 'Slider';
$route['slider'] = 'Slider';
$route['(ar|en)/slider/(:any)'] = 'Slider/$1';
$route['slider/(:any)'] = 'Slider/$1';
$route['(ar|en)/slider/(:any)/(:any)']= 'Slider/$1/$2';
$route['slider/(:any)/(:any)']= 'Slider/$1/$2';
//--------------- about   ----------------------
$route['(ar|en)/about'] = 'About';
$route['about'] = 'About';
$route['(ar|en)/about/(:any)'] = 'About/$1';
$route['about/(:any)'] = 'About/$1';
$route['(ar|en)/about/(:any)/(:any)']= 'About/$1/$2';
$route['about/(:any)/(:any)']= 'About/$1/$2';

//--------------- sub-about   ----------------------
$route['(ar|en)/sub-about'] = 'SubAbout';
$route['sub-about'] = 'SubAbout';
$route['(ar|en)/sub-about/(:any)'] = 'SubAbout/$1';
$route['sub-about/(:any)'] = 'SubAbout/$1';
$route['(ar|en)/sub-about/(:any)/(:any)']= 'SubAbout/$1/$2';
$route['sub-about/(:any)/(:any)']= 'SubAbout/$1/$2';


