<?php
class Web extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        //--------------------------------------------------------
        $this->load->model('system_management/Setting_model');
        $this->load->model('News_model');
        $this->load->model('Photos_model');
         $this->load->model('Videos_model');
        $this->setting = $this->Setting_model->getSettings();
        $this->news = $this->News_model->order_by("date","DESC")->limit(2)->get_all();
        // $this->news = $this->News_model->limit(2)->get_all();

        //--------------------------------------------------------
         $this->lang->load('static', 'ar');
        $this->webLang = 'ar';
        /*
        $lang = $this->uri->segment(1);

        if(in_array($lang,array("ar","en"))){
            $this->webLang = $lang ;
            if ($lang == "ar") {
                $this->lang->load('static', 'ar');
                delete_cookie("last_lang");
                 set_cookie('last_lang',"ar",time()+86400*30);
            }else{
                $this->lang->load('static', 'en');
                delete_cookie("last_lang");
                 set_cookie('last_lang',"en",time()+86400*30);
            }
        }
        else{
            $cookeLang  = $this->input->cookie('last_lang');
            if(isset($cookeLang)){
                $this->webLang = $cookeLang;
                $this->lang->load('static', $cookeLang);
            }
            else{
            $this->webLang = 'ar' ;
            $this->lang->load('static', 'ar');
                set_cookie('last_lang',"ar",time()+86400*30);
            }
        }
        //-------------------------------------------------------
        //-------------------------------------------------------
        $cookeLastVisit  = $this->input->cookie('last_visit');
        if(isset($cookeLastVisit)){
            delete_cookie("last_visit");
            set_cookie('last_visit',strtotime(date("Y-m-d")),time()+86400*30);
        }
        else{
            set_cookie('last_visit',strtotime(date("Y-m-d")),time()+86400*30);
            $this->load->model('Model_visit');
            $this->Model_visit->insertVisitor(date("Y-m-d"),"web_count");
        }

         */
    }
    private function test($data = array()){
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        die;
    }
    private function test_j($data = array()){
        header('Content-Type: application/json');
        echo  json_encode ($data);
        die;
    }

    private function setPaging($controller,$total_records, $limit_per_page){
        $this->load->library('pagination');
        $config['base_url'] = base_url() . $controller;
        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit_per_page;
        $config["uri_segment"] = 3;
        $config['num_links'] = 2;
        $config['use_page_numbers'] = false;
        $config['reuse_query_string'] = true;
        $config['full_tag_open'] = '<ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul>';
        $config['num_links'] = 5;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'my_page';
        $config['full_tag_open'] = '<ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul>';

        $config['page_query_string'] = true;
		$config['enable_query_strings'] = true ;
        $config['prev_link'] = '&lt;السابق  '; // السابق
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = 'التالي &gt;';  // التالى
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }
    /*
    private function manegeShowCookie($name ,$fild){
        $cookeMainCat  = $this->input->cookie($name);
        if(isset($cookeMainCat)){
            if($cookeMainCat != strtotime(date("Y-m-d"))){
                delete_cookie($name);
                set_cookie($name,strtotime(date("Y-m-d")),time()+86400*30);
                $this->load->model('Model_visit');
                $this->Model_visit->insertVisitor(date("Y-m-d"),$fild);
            }
        }else{
            set_cookie($name,strtotime(date("Y-m-d")),time()+86400*30);
            $this->load->model('Model_visit');
            $this->Model_visit->insertVisitor(date("Y-m-d"),$fild);
        }
    }
    */
    /*
    private function sendEmail($data,$from,$to){
        //Load email library
        $emailConfig = array(
            'protocol' => 'mail',
            'smtp_port' => 25,
            'mailtype' => 'html',
            'validate' => true,
            'charset' => 'utf-8'
        );
        //------------------------------------
        $from = array(
            'email' => $from,
            'name' => 'موقع شركــــــــــة ماوان الزراعــيــــــــة'
        );
        $to = array($to);
        $subject = ' شركــــــــــة ماوان الزراعــيــــــــة';
        $message = $this->load->view('frontend/requires/message_email', $data, true);
        //------------------------------------
        $this->load->library('email', $emailConfig);
        $this->email->set_newline("\r\n");
        $this->email->from($from['email']);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->set_mailtype("html");
        // Ready to send email and check whether the email was successfully sent
        if (!$this->email->send()) {
            // Raise error message
            return show_error($this->email->print_debugger());
            // return 0;
        } else {
            // Show success notification or other things here
            return 'Success to send email';
        }
    }
    */

    /**
     *  ============================================================
     *
     *  ------------------------------------------------------------
     *
     *  ============================================================
     */
    public function index(){
        $this->load->model('Slider_model');
        $this->load->model('Branchs_model');
        $this->load->model('Team_work_model');
        $this->load->model('Site_texts_model');
        $this->load->model('Partners_model');

         $data["sliders"] = $this->Slider_model->get_all();
         $data["branches"] = $this->Branchs_model->limit(6)->get_all();
         $data["team"] = $this->Team_work_model->get_all();
         $data["features"] = $this->Site_texts_model->limit(4)->get_many_by(["parent_id"=>6]);
         $data["partners"] = $this->Partners_model->get_all();
         
         
         //========= SHARE ===================
         $data["share_title"]="مجموعة المتسابق السعودي";
         $data["share_content"]="تعتبر وكالة المتسابق السعودي للدعاية والإعلان والتسويق التي أسسها رجل الاعمال وبطل الشرق الأوسط للراليات / د. محمد بن احمد المالكي في منتصف الثمانينات الميلادية هي اللبنة الأولى لمجموعة المتسابق السعودي القابضة";
         //$logo= $data["profile_data"]->logo;
         //$data["share_image"]="uploads/images/".$logo;
        /* echo "<pre>";
         print_r($data["share_image"]);
         die;*/

        //========= SHARE ===================
        
        
         
         $data["my_footer"] = ['revolution'];
         $data['subview'] = 'home';
         $this->load->view('layout/web', $data);
    }

    public  function about_us(){
        $this->load->model('Site_texts_model');
        $data["about"]   = $this->Site_texts_model->with('subs')->get(1);
        $data["version"] = $this->Site_texts_model->with('subs')->get(2);
        $data["message"] = $this->Site_texts_model->with('subs')->get(3);
        $data["basic"]   = $this->Site_texts_model->with('subs')->get(4);
        $data["goals"]   = $this->Site_texts_model->with('subs')->get(5);
        $data["spacial"] = $this->Site_texts_model->with('subs')->get(6);
        $data['subview'] = 'about_us';
        $this->load->view('layout/web', $data);
    }

    public function read_file() {
            $file_name = $this->input->get('file');
            // $file_name = 'ed97631c96f672815a43b72e7e594090.pdf';
            $this->load->helper('file');
            $path_iamge = './uploads/images/';
            $path_files = './uploads/files/';
            if (file_exists($path_iamge . $file_name)) {
                $main_path = $path_iamge;
            } elseif (file_exists($path_files . $file_name)) {
                $main_path = $path_files;
            }
            $file_path = $main_path.$file_name;
            header('Content-Type: application/pdf');
            header('Content-Discription:inline; filename="'.$file_name.'"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges:bytes');
            header('Content-Length: ' . filesize($file_path));
            readfile($file_path);
        }

    public  function contactUs(){
        $data['subview'] = 'contact';
        $this->load->view('layout/web', $data);
    }

    public function contact(){
        if($this->input->post('name')  && $this->input->post('email') &&
        $this->input->post('subject')  && $this->input->post('message')
        ){

            $this->load->model('Contacts_model');
            $Idata['name'] = $this->input->post('name') ;
            $Idata['email'] = $this->input->post('email') ;
            $Idata['subject'] = $this->input->post('subject') ;
            $Idata['message'] = $this->input->post('message') ;
            $Idata['date'] = time();
            $this->Contacts_model->insert($Idata);
            //-----------------------------------
           // $this->sendEmail($Idata,$Idata['email'],$this->website->message_email);
            //-----------------------------------
            $result =  ($this->webLang == "ar")? "شكرا لتواصلك معنا":"Thank for contact with us";
            echo $result;
        }
        else{
        //   echo 2 ;
        }
    }

    public  function news(){
        $this->load->model('News_model');
        //----------------------------
        $limitPerPage = 10;
        $page = (($this->input->get('my_page'))) ? $this->input->get('my_page') - 1 : 0;
        $total_records = $this->News_model->count_all();
        if ($total_records > 0) {
            
            $data["news"]=$this->News_model->limit($limitPerPage,  $page)->order_by("date","DESC")->get_all();
            $data["links"]= $this->setPaging("our-news",$total_records,$limitPerPage);
        }
        //----------------------------
        $data['subview'] = 'news';
        $this->load->view('layout/web', $data);
    }

    public  function single_news(){
        $this->load->model('News_model');
        $id = $this->input->get("id");
        if(isset($id) && !empty($id)){
          $data['one'] = $this->News_model->with_meny(["user","subs"])->get($id);
          //$this->test_j($data);
            if (isset($data['one']) && !empty($data['one'])) {
                $data['news'] = $this->News_model->limit(10)->get_many_by(['id !='=>$id]);
                $data['subview'] = 'single_news';
                $this->load->view('layout/web', $data);
            }
            else {
                redirect("Page404", 'refresh');
            }
        }
        else{
            redirect("Page404", 'refresh');
        }
    }

    public  function branches(){
        $this->load->model('Branchs_model');
        //----------------------------
        $data["branches"]=$this->Branchs_model->get_all();
        //----------------------------
        $data['subview'] = 'branches';
        $this->load->view('layout/web', $data);
    }

    public  function singleBranch(){
        $this->load->model('Branchs_model');
        $id = $this->input->get("id");
        if(isset($id) && !empty($id)){
            $data['one'] = $this->Branchs_model->with("subs")->get($id);
            if (isset($data['one']) && !empty($data['one'])) {
                $data['subview'] = 'single_branch';
                $this->load->view('layout/web', $data);
            }
            else {
                redirect("Page404", 'refresh');
            }
        }
        else{
            redirect("Page404", 'refresh');
        }
    }
    
    
        public function gallery()
    {
        $data['library'] = $this->Photos_model->display_imgs();

        $data['subview'] = 'albums';
        $this->load->view('layout/web', $data);
    }


     public function album($id){
      
            $data['album'] = $this->Photos_model->display_album($id);
      
        $data['subview'] = 'gallery';
        $this->load->view('layout/web', $data);
    }
    public function videos()
    {
        $data['videos'] = $this->Videos_model->display_videos();


        $data['subview'] = 'videos';
        $this->load->view('layout/web',$data);
    }
    
      public  function single_team(){
        $this->load->model('Team_work_model');
        $id = $this->input->get("id");
        if(isset($id) && !empty($id)){
          $data['one'] = $this->Team_work_model->with_meny(["user","subs"])->get($id);
          //$this->test_j($data);
            if (isset($data['one']) && !empty($data['one'])) {
               
                $data['subview'] = 'single_team';
                $this->load->view('layout/web', $data);
            }
            else {
                redirect("Page404", 'refresh');
            }
        }
        else{
            redirect("Page404", 'refresh');
        }
    }

}// END CLASS 
