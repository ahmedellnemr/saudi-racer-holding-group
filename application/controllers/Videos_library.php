<?php
class Videos_library extends MY_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->helper(array('url','text','form'));
       
        $this->load->model('Videos_model');
        error_reporting(E_ALL & ~E_NOTICE);
    }


   


    public function videos(){ //Videos_library/videos
        $data['get_videos'] = $this->Videos_model->display_videos();
        if ($this->input->post('ADD')){
            $this->Videos_model->insert_video();

            $this->message('success','تم الاضافة بنجاح');
            redirect('Videos_library/videos','refresh');

        }
        
          $data["my_footer"] = ['table'];
        $data['subview'] = 'library/videos_library_view';
        $this->load->view('layout/admin', $data);
        
        
    }

    public function get_videos(){ // Videos_library/get_videos
        $data['lenght']= $_POST['length'];

        $this->load->view('backend/library/get_videos');
        
        
    }
    public function Delete($id){  // Videos_library/Delete
        $this->Videos_model->delet_video($id);
        $this->message('success','تم الحذف بنجاح');
        redirect('Videos_library/videos','refresh');
    }
//    public function Update($id){ // admin/library/Videos_library/Update
//        $data['get_video'] = $this->Videos_model->get_by_id($id);
//      //  $this->test( $data['get_video']);
//
//        $data['subview'] = 'admin/library/videos_library_view';
//        $this->load->view('layout/admin', $data);
//    }

public function get_Main_video(){
        $this->Videos_model->get_Main_video();
        echo json_encode($_POST);
    }




}