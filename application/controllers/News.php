<?php
class News extends MY_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model('News_model','Cmodel');
        $this->load->model('News_images_model');
       //  redirect("Page404", 'refresh');
        /*
         $main_iamge = $this->upload_image("");
            $Idata[""] = $main_iamge;
            $filesCount = $_FILES["images"]['size'][0];
            if ($filesCount != "0" || $filesCount != 0) {
                $imgs = $this->upload_muli_image("images");
                $this->Model_salon->add_images($id, $imgs);
            }

         $main_iamge = $this->upload_image("");
            if (!empty($main_iamge)) {
                $Idata[""] = $main_iamge;
            }

         */
    }

    public  function index(){

        $data["data_table"] = $this->Cmodel->order_by('date', 'DESC')->get_all();
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'المركز الاعلامى';
        $data["my_footer"] = ['table'];
        $data['subview'] = 'news/all';
        $this->load->view('layout/admin', $data);
    }

    public  function add(){

        $data["op"] = 'INSERT';
        $data["form"] = 'news/create';
        $data["out"] = $this->Cmodel->get_filds();
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'المركز الاعلامى';
        $data["my_footer"] = ["upload","valid","multi_upload"];
        $data['subview'] = 'news/one';
        $this->load->view('layout/admin', $data);
    }

    public  function edit($id){

        $data["op"] = 'UPDTATE';
        $data["form"] = 'news/update/'.$id;
        $data["out"] = $this->Cmodel->as_array()->get($id);
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'المركز الاعلامى';
        $data["my_footer"] = ["upload","valid","date"];
        $data['subview'] = 'news/one';
        $this->load->view('layout/admin', $data);
    }

    public  function create(){

        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('Pdata');
            $logo = $this->upload_image("logo");
            $Idata["logo"] = $logo;
            $Idata["date"] = strtotime($Idata["date"]) ;
            $id = $this->Cmodel->insert($Idata);
            $filesCount = $_FILES["images"]['size'][0];
            if ($filesCount != "0" || $filesCount != 0) {
                $imgs = $this->upload_muli_image("images");
                if (!empty($imgs) && $imgs != null) {
                    foreach ($imgs as $key=>$value ) {
                        $dataIm["main_id"] = $id;
                        $dataIm["name"] = $value;
                        $this->News_images_model->insert($dataIm);
                    }
                }
            }
            //----------------------------------------------
            $this->message('s');
            redirect("news/add", 'refresh');
        }

    }

    public  function update($id){

        if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Idata = $this->input->post('Pdata');
            $logo = $this->upload_image("logo");
            if (!empty($logo)) {
                $Idata["logo"] = $logo;
            }
            $Idata["date"] = strtotime($Idata["date"]) ;
            $this->Cmodel->update($id,$Idata);
            $filesCount = $_FILES["images"]['size'][0];
            if ($filesCount != "0" || $filesCount != 0) {
                $imgs = $this->upload_muli_image("images");
                if (!empty($imgs) && $imgs != null) {
                    foreach ($imgs as $key=>$value ) {
                        $dataIm["main_id"] = $id;
                        $dataIm["name"] = $value;
                        $this->News_images_model->insert($dataIm);
                    }
                }
            }
            //----------------------------------------------
            $this->message('i');
            redirect("news", 'refresh');
        }

    }


    public  function delete($id){

        $this->Cmodel->delete($id);
        $this->message('e');
        redirect("news", 'refresh');
    }



} //END CLASS
?>