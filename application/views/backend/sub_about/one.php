<?php if($op == 'UPDTATE' ):
    $out['input']='UPDTATE';
    $out['input_title']='تعديل ';
else:
    $out['input']='INSERT';
    $out['input_title']='حفظ ';
endif?>

<?=form_open_multipart($form,["class"=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed']);?>

    <div class="m-portlet__body">
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label>العنوان الرئيسى   :</label>
                <select name="Pdata[parent_id]" class="form-control m-input" id="exampleSelect1"
                        data-validation="required" aria-required="true" >
                    <option value="">اختر </option>
                    <?php
                    $arr = getSitText();
                    if(!empty($arr)){
                        foreach ($arr as $row):
                            if($row->type == 0){ continue;}?>
                            <option value="<?=$row->id?>" <?=($out["parent_id"] == $row->id)? "selectet":""?>><?=$row->title?></option>
                        <?php endforeach;
                    }?>

                </select>
            </div>
            <div class="col-lg-6">
                <label class="">العنوان :</label>
                <input type="text" name="Pdata[name]" value="<?= $out['name'] ?>" class="form-control  m-input"
                       data-validation="required">
            </div>

        </div>
        <div class="form-group m-form__group row">
            <div class="col-lg-8">
                <label class="">  التفاصيل :</label>
                <textarea name="Pdata[details]" class="form-control" data-provide="markdown"
                          rows="10"><?= $out['details'] ?></textarea>
            </div>

            <div class="col-lg-4">
                <label class="">الصورة  :</label>
                <?php if($op == 'UPDTATE' ){ ?>
                    <input type="file" id="input-file-now-custom-1" name="logo" class="dropify"
                           data-default-file="<?php echo base_url() . IMAGEPATH . $out['logo'] ?>"/>
                <?php } else { ?>
                    <input type="file" id="input-file-now-custom-1" name="logo" class="dropify" data-validation="required"/>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
        <div class="m-form__actions m-form__actions--solid">
            <div class="row">
                <div class="col-lg-6">
                    <button type="submit" name="<?php echo $out['input']?>" value="<?php echo $out['input']?>"
                            class="btn btn-primary">
                        <span><i class="fa fa-floppy-o" aria-hidden="true"></i></span> <?php echo $out['input_title']?>
                    </button>
                    <!--     <button type="reset" class="btn btn-secondary">Cancel</button>-->
                </div>
                <div class="col-lg-6 m--align-right">
                    <!--  <button type="reset" class="btn btn-danger">Delete</button>-->
                </div>
            </div>
        </div>
    </div>
<?= form_close()?>