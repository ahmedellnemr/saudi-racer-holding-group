<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1"
     m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
    <?php $dir = ($this->AdminLang == "ar")? "la-angle-left":"la-angle-right"; ?>
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
        <!--<li class="m-menu__item  m-menu__item--active" aria-haspopup="true">
            <a href="<?/*=base_url().'Dashboard'*/?>" class="m-menu__link ">
                <i class="m-menu__link-icon flaticon-line-graph"></i>
                <span class="m-menu__link-title">
                    <span  class="m-menu__link-wrap">
                        <span class="m-menu__link-text">
                            <?/*=lang("home")*/?>
                        </span>

                    </span>
                </span>
            </a>
        </li>-->
        <?php if(isset($_SESSION['is_developer']) && $_SESSION['is_developer'] == 1){?>
       <!-- <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-layers"></i>
                <span class="m-menu__link-text">إدارة النظام </span>
                <i class="m-menu__ver-arrow la <?/*=$dir*/?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">إدارة النظام</span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?/*=base_url().""*/?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span></i>
                            <span class="m-menu__link-text">الإدارات</span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?/*=base_url().""*/?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span></i>
                            <span class="m-menu__link-text">الصفحات</span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?/*=base_url().""*/?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span></i>
                            <span class="m-menu__link-text">الصلاحيات</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>-->
        <?php }?>
        <!--
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">المركز الاعلامي</span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">المركز الاعلامي</span>
                        </span>
                    </li>
                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">level 2</span>
                            <i class="m-menu__ver-arrow la <?=$dir?>"></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="#" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">level 3</span></a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="#" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">level 2</span></a>
                    </li>
                </ul>
            </div>
        </li>
        -->
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">البيانات الاساسية </span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">البيانات الاساسية </span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."main-setting"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">بيانات الموقع</span></a>
                    </li>

                </ul>
            </div>
        </li>
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">المركز الاعلامي</span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">المركز الاعلامي</span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."news"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">عرض الاخبار</span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."news/add"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">إضافة خبر</span></a>
                    </li>
                </ul>
            </div>
        </li>
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">شركاء النجاح</span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">شركاء النجاح</span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."partner"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">عرض الشركاء</span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."partner/add"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">إضافة شركاء</span></a>
                    </li>
                </ul>
            </div>
        </li>
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">فروعنا</span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">فروعنا</span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."branch"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">عرض الفروع</span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."branch/add"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">إضافة فرع</span></a>
                    </li>
                </ul>
            </div>
        </li>
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">فريق العمل</span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">فريق العمل</span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."team-work"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">عرض فريق العمل</span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."team-work/add"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">إضافة موظف </span></a>
                    </li>
                </ul>
            </div>
        </li>
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">البنر الرئيسى</span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">البنر الرئيسى</span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."slider"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">عرض </span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."slider/add"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">إضافة  </span></a>
                    </li>
                </ul>
            </div>
        </li>
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">عن المجموعة </span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">عن المجموعة </span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."about"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">عرض  الرئيسى </span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."about/add"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">إضافة الرئيسى  </span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."sub-about"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">عرض المقاطع الفرعية </span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."sub-about/add"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">إضافة مقاطع فرعية  </span></a>
                    </li>
                </ul>
            </div>
        </li>
        <!------------------------------------------>
        
        
         
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text"> مكتبة الصور والفيديوهات </span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">مكتبة الصور والفيديوهات </span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."Photos_library/photos"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">ألبومات الصور </span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."Videos_library/videos"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">مكتبة الفيديوهات </span></a>
                    </li>
                    
                </ul>
            </div>
        </li>
        <!------------------------------------------>
        
    </ul>
</div>