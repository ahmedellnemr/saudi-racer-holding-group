<!-- end:: Body -->

<!-- begin::Footer -->
<footer class="m-grid__item		m-footer ">
    <div class="m-container m-container--fluid m-container--full-height m-page__container">
        <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
            <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
						<!--	<span class="m-footer__copyright">
								<?/*=date("Y")*/?> &copy; Metronic theme by <a href="#" class="m-link">Keenthemes</a>
							</span>-->
            </div>
            <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
               <!-- <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">About</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Privacy</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">T&C</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Purchase</span>
                        </a>
                    </li>
                    <li class="m-nav__item m-nav__item">
                        <a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Support Center" data-placement="left">
                            <i class="m-nav__link-icon flaticon-info m--icon-font-size-lg3"></i>
                        </a>
                    </li>
                </ul>-->
            </div>
        </div>
    </div>
</footer>

<!-- end::Footer -->
</div>

<!-- end:: Page -->

<!-- begin::Quick Sidebar -->
<?php  // $this->load->view('backend/requires/second_sidbar');?>
<!-- end::Quick Sidebar -->

<!-- begin::Scroll Top -->
<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
</div>

<!-- end::Scroll Top -->

<!-- begin::Quick Nav -->
<?php  // $this->load->view('backend/requires/quick_nav');?>
<!-- begin::Quick Nav -->

<!--begin::Global Theme Bundle -->
<script src="<?= base_url() . ADMINASSETS ?>vendors/base/vendors.bundle.js?v=<?=VER?>" type="text/javascript"></script>
<script src="<?= base_url() . ADMINASSETS ?>demo/default/base/scripts.bundle.js?v=<?=VER?>" type="text/javascript"></script>
<!--end::Global Theme Bundle -->
<!--begin::Page Vendors -->
<?php if (isset($my_footer)) { ?>



    <!----------------------------------------------------------->
    <?php  if(in_array("multi_upload",$my_footer)){?>
        <script type="text/javascript" src="<?=base_url().ASS?>multi-upload/imageuploadify.min.js?v=<?=VER?>"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.multi-up').imageuploadify();
            })
        </script>
    <?php }  ?>
    <!----------------------------------------------------------->
    <?php if (in_array("date", $my_footer)): ?>
        <script src="<?=base_url().ASS?>date-picker/jquery.datetimepicker.full.js?v=<?=VER?>"></script>
        <script>
            $('.datepicker-m').datetimepicker({
                format:'Y-m-d',
                time: false
            });
        </script>
    <?php endif ?>
    <!----------------------------------------------------------->
    <?php if (in_array("upload",$my_footer)): ?>
        <!-- file upload -->
        <script src="<?=base_url().ASS ?>dropify/js/jasny-bootstrap.js?v=<?=VER?>"></script>
        <!-- This is data table -->
        <!-- jQuery file upload -->
        <script src="<?=base_url().ASS ?>dropify/dist/js/dropify.min.js?v=<?=VER?>"></script>
        <script>
            $(document).ready(function () {
                // Basic
                $('.dropify').dropify();
                // Translated
                $('.dropify-fr').dropify({
                    messages: {
                        default: 'Glissez-d�posez un fichier ici ou cliquez',
                        replace: 'Glissez-d�posez un fichier ou cliquez pour remplacer',
                        remove: 'Supprimer',
                        error: 'D�sol�, le fichier trop volumineux'
                    }
                });
                // Used events
                var drEvent = $('#input-file-events').dropify();
                drEvent.on('dropify.beforeClear', function (event, element) {
                    return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                });
                drEvent.on('dropify.afterClear', function (event, element) {
                    alert('File deleted');
                });
                drEvent.on('dropify.errors', function (event, element) {
                    console.log('Has Errors');
                });
                var drDestroy = $('#input-file-to-destroy').dropify();
                drDestroy = drDestroy.data('dropify')
                $('#toggleDropify').on('click', function (e) {
                    e.preventDefault();
                    if (drDestroy.isDropified()) {
                        drDestroy.destroy();
                    } else {
                        drDestroy.init();
                    }
                })
            });
        </script>
    <?php endif ?>
    <!----------------------------------------------------------->
    <?php if (in_array("table", $my_footer)) { ?>.

        <!-- datatables-->
        <script src="<?= base_url().ASS ?>mydatatables/js/jquery.dataTables.min.js?v=<?=VER?>"></script>
        <script src="<?= base_url().ASS ?>mydatatables/js/dataTables.buttons.min.js?v=<?=VER?>"></script>
        <script src="<?= base_url().ASS ?>mydatatables/js/buttons.flash.min.js?v=<?=VER?>"></script>
        <script src="<?= base_url().ASS ?>mydatatables/js/jszip.min.js?v=<?=VER?>"></script>
        <script src="<?= base_url().ASS ?>mydatatables/js/pdfmake.min.js?v=<?=VER?>"></script>
        <script src="<?= base_url().ASS ?>mydatatables/js/vfs_fonts.js?v=<?=VER?>"></script>
        <script src="<?= base_url().ASS ?>mydatatables/js/buttons.html5.min.js?v=<?=VER?>"></script>
        <script src="<?= base_url().ASS ?>mydatatables/js/buttons.print.min.js?v=<?=VER?>"></script>
        <script src="<?= base_url().ASS ?>mydatatables/js/buttons.colVis.min.js?v=<?=VER?>"></script>
        <script src="<?= base_url().ASS ?>mydatatables/js/dataTables.responsive.min.js?v=<?=VER?>" id="responsive-dt"></script>
        <script src="<?= base_url().ASS ?>mydatatables/js/plugin.js?v=<?=VER?>"></script>
    <?php } ?>
    <!----------------------------------------------------------->
    <?php if (in_array("valid",$my_footer)): ?>
        <script src="<?= base_url().ASS ?>validator/jquery.form-validator.js?v=<?=VER?>"></script>
        <script>
            $(function () {
                $.validate({
                    validateHiddenInputs: true
                });
            });
        </script>
    <?php endif ?>
    <!----------------------------------------------------------->

<?php } ?>
<!--end ::Page Vendors -->



<!--
<script src="<?=base_url().ASS ?>ckeditor/ckeditor.js"></script>
<script src="<?=base_url().ASS ?>ckeditor/samples/js/sample.js"></script>
<script>
    initSample();
    $('.editor').each(function(e){
        CKEDITOR.replace( this.id, { customConfig: '/jblog/ckeditor/config_Large.js' });
    });
</script>-->


</body>

<!-- end::Body -->
</html>