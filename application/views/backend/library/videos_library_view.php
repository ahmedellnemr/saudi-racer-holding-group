<style type="text/css">
    .table thead th {
        background-color: #6572b8;
        color: #fff;
    }
</style>
<div class="col-12 col-lg-12 col-sm-12 " >
  <?php
            if(isset($get_video) && $get_video!=null){
                $form = form_open_multipart("Videos_library/Update/".$get_video->id);
            } else{
                $form =form_open_multipart("Videos_library/videos");
            }
            ?>
            <?php echo  $form;
            ?>
            
            
   <div class="m-portlet__body">
    <div class="row">
 
          
            <div class="form-group col-12 col-lg-12 col-sm-12 no-padding">
            
            <p>اضغط هنا على هذا الزر لإضافة فيديو جديد , ملحوظة يضاف لينك فيديو من اليوتيوب</p>
                <input type="hidden" id="count_row" value="0" />
                <button type="button" class="btn btn-success btn-next add_attchments"
                        onclick="add_row_video()"
            
                >   اضافة فيديو   <i class="fa fa-plus" aria-hidden="true"></i></button><br><br>
            
            </div>
            
            <div class="form-group col-12 col-lg-12 col-sm-12 no-padding">
            
            
            
                <table class="table table-bordered"   id="videotable"  >
                    <thead >
                    <tr class="success">
                        <th>م</th>
                        <th>عنوان الفيديو</th>
                        <th style="text-align: center">لينك الفيديو من اليوتيوب </th>
            
                        <th style="text-align: center">الإجراء</th>
                    </tr>
                    </thead>
                    <tbody id="result_video">
            
            
            
                    </tbody>
                </table>
            
            
            </div>
            <div class="form-group col-12  col-lg-12 col-sm-12 text-center no-padding">
                
                
                <button type="submit" name="ADD"  value="حفظ " class="btn btn-success btn-next ">   <i class="fa fa-save" aria-hidden="true"></i> حفظ</button>
                        
            
            </div>

 

</div>

</div>


        <?php
        echo  form_close();
        ?>
        
</div>
<?php
if (isset($get_videos) && !empty($get_videos) ){
    $x = 1;


?>
<div class="col col-lg-12 col-md-12 col-sm-12">
    
            <table id="myTable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                 
                    <th>م</th>
                    <th style="width: 200px;">عنوان الفيديو</th>

                    <th>الفيديو</th>

                    <th style="width: 50px;">الاجراء</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($get_videos as $video){
                    ?>
                    <tr>
                       
                        <td><?= $x++?></td>
                        <td><?= word_limiter($video->video_title,25)?></td>
                        <td>
                            <iframe width="40%" height="100" src="https://www.youtube.com/embed/<?= $video->video_link?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

                        </td>
                        <td>
                          
                                
                                <a href="<?=base_url()."Videos_library/Delete/".$video->id?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                                <button type="button" class="btn m-btn--pill btn-danger btn-sm" title="حذف">
                                                    <i class="fa fa-trash-alt fa-xs"> </i> </button></a>

                        </td>
                    </tr>
                <?php
                }
                ?>
                </tbody>

            </table>



       
</div>
<?php
}
?>


<script>
    function add_row_video(){
      //  $("#videotable").show();
        $("#empty").remove();

        var x = document.getElementById('result_video');
        var len = x.rows.length +1;
        var dataString   ='length=' + len;

        $.ajax({
            type:'post',
            url: '<?php echo base_url() ?>Videos_library/get_videos',
            data:dataString,
            dataType: 'html',

            cache:false,
            success: function(html){

                $("#result_video").append(html);

            }
        });
    }


</script>

 



