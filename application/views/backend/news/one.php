<?php if($op == 'UPDTATE' ):
    $out['input']='UPDTATE';
    $out['input_title']='تعديل ';
else:
    $out['input']='INSERT';
    $out['input_title']='حفظ ';
endif?>

<?=form_open_multipart($form,["class"=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed']);?>

<div class="m-portlet__body">
    <div class="form-group m-form__group row">
        <div class="col-lg-6">
            <label>عنوان الخبر :</label>
            <input type="text" name="Pdata[title]" value="<?= $out['title'] ?>" class="form-control m-input"
                   data-validation="required">
        </div>
        <div class="col-lg-6">
            <label class="">تاريخ الخبر :</label>
           <!-- <input type="email" name="Pdata[date]" value="<?/*= $out['date'] */?>" class="form-control m-input"
                   data-validation="required">-->
            <input type="text" name="Pdata[date]" class="form-control datepicker-m m-input"
                   value="<?=($op == 'UPDTATE')? date("Y-m-d",$out['date']) :""?>" data-validation="required">
        </div>

    </div>
    <div class="form-group m-form__group row">
        <div class="col-lg-8">
            <label class="">تفاصيل الخبر :</label>
            <textarea name="Pdata[details]" class="form-control editor" data-provide="markdown" 
                      rows="10"><?= $out['details'] ?></textarea>
        </div>

        <div class="col-lg-4">
            <label class="">صورة الخبر :</label>
            <?php if($op == 'UPDTATE' ){ ?>
                <input type="file" id="input-file-now-custom-1" name="logo" class="dropify"
                       data-default-file="<?php echo base_url() . IMAGEPATH . $out['logo'] ?>"/>
            <?php } else { ?>
                <input type="file" id="input-file-now-custom-1" name="logo" class="dropify" data-validation="required"/>
            <?php } ?>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <input type="file" name="images[]" class="multi-up"  accept="image/*" multiple>
    </div>
</div>

<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
    <div class="m-form__actions m-form__actions--solid">
        <div class="row">
            <div class="col-lg-6">
                <button type="submit" name="<?php echo $out['input']?>" value="<?php echo $out['input']?>"
                        class="btn btn-primary">
                    <span><i class="fa fa-floppy-o" aria-hidden="true"></i></span> <?php echo $out['input_title']?>
                </button>
           <!--     <button type="reset" class="btn btn-secondary">Cancel</button>-->
            </div>
            <div class="col-lg-6 m--align-right">
              <!--  <button type="reset" class="btn btn-danger">Delete</button>-->
            </div>
        </div>
    </div>
</div>
<?= form_close()?>









