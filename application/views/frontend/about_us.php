<!---------------------------------------   about   --------------------------------------------------->
        <?php if (isset($about) && !empty($about)):
           $about_title = $about->name;
           $about_detals = $about->details;
            $about_img    = base_url().IMAGEPATH.$about->logo;
        else:
            $about_title = " نبذة عن المجموعة";
            $about_detals = 'تعتبر وكالة المتسابق السعودي للدعاية والإعلان والتسويق التي أسسها رجل الاعمال وبطل الشرق الأوسط للراليات / د. محمد بن احمد المالكي في منتصف الثمانينات الميلادية هي اللبنة الأولى لمجموعة المتسابق السعودي القابضة ومن ثم عملنا على إنشاء مؤسسة المتسابق السعودي للإنتاج الفني تلاها تأسيس الفريق السعودي الأول لرياضة السيارات برعاية الرئيس العام لرعاية الشباب صاحب السمو الملكي الأمير / فيصل بن فهد بن عبدالعزيز (رحمه الله) وامتدادا لدعم وتطوير رياضة السيارات انشأنا مركز المتسابق السعودي لصيانة السيارات وتعديلها كما وعملنا على إنشاء أثير الوسطى لتقنية المعلومات و أثير الوسطى لتنظيم المعارض والمؤتمرات والدورات ونعمل في مجموعة المتسابق السعودي القابضة على ابتكار وتنفيذ كل ما يتعلق بهذه المجالات وفق معايير عالمية تواكب رؤية 2030 حيث نعمل وفق نماذج خاصة وعالمية لتحقيق الأهداف الاستراتيجية لشركائنا';
            $about_img    = base_url().WEBASSETS."img/logo.png";
        endif;
        ?>
<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
    <div class="section-header container">
        <div class="container-fluid">
            <h3>
              <?=$about_title?>
            </h3>
        </div>
    </div>
</section>
<section class="about-us ptop-20 pbottom-40">

    <div class="container-fluid">
        <div class="about-logo text-center wow fadeInUp">
            <img src="<?=$about_img?>">
        </div>
    </div>
    <div class="container">
        <div class="nbza text-center wow fadeInDown">
            <p> <?=$about_detals?></p>
        </div>
    </div>
</section>
<!------------------------------------------------------------------------------------------>


<section class="vision pbottom-40">
    <div class="container-fluid">
        <div class="col-md-6 col-sm-6 col-xs-12 fadeInUp wow" data-wow-delay=".3s">
            <div class="more-about ">
                <?php if (isset($version) && !empty($version)):
                    $version_title = $version->name;
                    $version_detals = $version->details;
                    $version_img = base_url() . IMAGEPATH . $version->logo;
                else:
                    $version_title = " رؤيتنا";
                    $version_detals = ' نقوم بتقديم وتطوير كل ما يتناسب مع شركائنا ونضيف قيمة متميزة لعملائنا وموظفينا ولكافة أصحاب المصالح .';
                    $version_img = base_url() . WEBASSETS . "img/icons/vision.png";
                endif;
                ?>
                <img src="<?= $version_img ?>">
                <div class="clrbg-before">
                    <h2 class="title-1"><?= $version_title ?>: </h2>
                    <div class="pad-10"></div>
                    <p><?= $version_detals ?></p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 fadeInUp wow" data-wow-delay=".8s" data-wow-duration="1s">
            <div class="more-about ">
                <?php if (isset($message) && !empty($message)):
                    $message_title = $message->name;
                    $message_detals = $message->details;
                    $message_img = base_url() . IMAGEPATH . $message->logo;
                else:
                    $message_title = " رسالتنا";
                    $message_detals = ' نسعى بشكل دائم، من خلال خبرتنا الواسعة، الى تطبيق كل ما هو لازم لتحقيق رؤيتنا، عبر نماذج فريدة، وسنقوم بالوفاء بالتزاماتنا تجاه أصحاب المصالح، الذين نعمل معهم في إطار مجموعة من القيم الجوهرية، التي نتقاسمها مع كل شريك من شركائنا.';
                    $message_img = base_url() . WEBASSETS . "img/icons/message.png";
                endif;
                ?>
                <img src="<?= $message_img ?>">
                <div class="clrbg-before">
                    <h2 class="title-1"> <?= $message_title ?>:</h2>
                    <div class="pad-10"></div>
                    <p><?= $message_detals ?></p>
                </div>
            </div>
        </div>
    </div>
</section>

<!------------------------------------------------------------------------------------------>





<section class="our-values ptop-50 pbottom-40">
    <div class="container-fluid">
        <div class="col-md-12 col-sm-12 col-xs-12 fadeInUp wow" data-wow-delay=".8s" data-wow-duration="1s">
            <div class="values ">

                <?php if (isset($basic) && !empty($basic)):
                    $basic_title = $basic->name;
                    $basic_detals = $basic->details;
                    $basic_img = base_url() . IMAGEPATH . $basic->logo;
                else:
                    $basic_title = " قيمنا";
                    $basic_detals = 'ننطلق من فلسفة عميقة ونعتمد تعزيز الجانب القيمي، وذلك وفق تعاليم الشريعة الإسلامية، حيث أننا نؤمن بإعطاء القيم والمبادئ الأولوية في أعمالنا، ان قيمنا التي نتخذها أساساً لأسلوب إدارة أعمالنا، و من بينها:';
                    $basic_img = base_url() . WEBASSETS . "img/icons/values.png";
                endif;
                ?>

                <div class="text-center">
                    <img src="<?=$basic_img?>">
                </div>

                <div class="text-center">
                    <h2 class="title-1"> <?=$basic_title?>:</h2>
                    <div class="pad-10"></div>
                    <p><?=$basic_detals?></p><br>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php if (isset($basic->subs) && !empty($basic->subs)):?>
                <?php foreach ($basic->subs as $row): ?>
                    <div class="col-md-20 col-sm-3 col-xs-6 text-center fadeInUp wow" data-wow-delay=".2s"
                         data-wow-duration="1s">
                        <div class="values-box">
                            <img src="<?= base_url() . IMAGEPATH .$row->logo?>">
                            <h5><?=$row->name?></h5>
                            <p><?=$row->details?></p>
                        </div>
                    </div>
                <?php endforeach ?>
            <?php else:?>
            <div class="col-md-20 col-sm-3 col-xs-6 text-center fadeInUp wow" data-wow-delay=".2s" data-wow-duration="1s">
                <div class="values-box">
                    <img src="<?=base_url().WEBASSETS?>img/icons/security.png">
                    <h5>سرية البيانات</h5>
                    <p>جوهر اهتمامنا، وقيمةٌ أساسية يلتزم بها جميع العاملين معنا داخليًا وخارجيًا</p>
                </div>
            </div>
            <div class="col-md-20 col-sm-3 col-xs-6 text-center fadeInUp wow" data-wow-delay=".4s" data-wow-duration="1s">
                <div class="values-box">
                    <img src="<?=base_url().WEBASSETS?>img/icons/amana.png">
                    <h5>الأمانة والنزاهة</h5>
                    <p>نتولى إدارة اعمالنا بنزاهة وامانة مقدمين عنصري المصداقية والوضوح على أي شيء اخر</p>
                </div>
            </div>
            <div class="col-md-20 col-sm-3 col-xs-6 text-center fadeInUp wow" data-wow-delay=".6s" data-wow-duration="1s">
                <div class="values-box">
                    <img src="<?=base_url().WEBASSETS?>img/icons/rough.png">
                    <h5>روح الفريق </h5>
                    <p>تتظافر جهودنا مع فريقنا سواء كانوا عملاء أو موردين أو شركاء أو حتى منافسين لأننا نؤمن بأهمية إقامة علاقات قوية تعتمد على التسامح والتفاهم والعمل لتحقيق المصلحة العامة.</p>
                </div>
            </div>
            <div class="col-md-20 col-sm-3 col-xs-6 text-center fadeInUp wow" data-wow-delay=".8s" data-wow-duration="1s">
                <div class="values-box">
                    <img src="<?=base_url().WEBASSETS?>img/icons/ingaz.png">
                    <h5>الانجاز </h5>
                    <p>نلتزم بالسعي لتحقيق اعلى المعايير الممكنة واجودها عبر أداء متفان ومنظم خلف الكواليس مع جميع أصحاب المصلحة يوازي النتائج التي تظهر على الواقع</p>
                </div>
            </div>
            <div class="col-md-20  col-sm-3 col-xs-6 text-center fadeInUp wow" data-wow-delay="1s" data-wow-duration="1s">
                <div class="values-box">
                    <img src="<?=base_url().WEBASSETS?>img/icons/support.png">
                    <h5>خدمة العملاء </h5>
                    <p> نوليها لعملائنا بتوفير الدعم الاستشاري والحلول المتخصصة في الوقت المطلوب</p>
                </div>
            </div>
            <?php endif; ?>

        </div>

    </div>

</section>

<section class="our-goals ptop-50 pbottom-40">
    <div class="container-fluid">

        <?php if (isset($goals) && !empty($goals)):
            $goals_title = $goals->name;
            $goals_detals = $goals->details;
            $goals_img = base_url() . IMAGEPATH . $goals->logo;
        else:
            $goals_title = " أهدافنا";
            $goals_detals = '';
            $goals_img = base_url() . WEBASSETS . "img/icons/aim.png";
        endif;
        ?>

        <div class="col-md-12 col-sm-12 col-xs-12 fadeInUp wow" data-wow-delay=".8s" data-wow-duration="1s">
            <div class="values ">
                <div class="text-center">
                    <img src="<?=$goals_img?>">
                </div>

                <div class="text-center">
                    <h2 class="title-1"> <?=$goals_title?>:</h2>
                    <div class="pad-10">
                        <p><?=$goals_detals?></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php if (isset($goals->subs) && !empty($goals->subs)): ?>
                <?php foreach ($goals->subs as $row): ?>
                    <div class="col-md-20 col-sm-3 col-xs-6 text-center fadeInUp wow" data-wow-delay=".2s"
                         data-wow-duration="1s">
                        <div class="values-box">
                            <img src="<?= base_url() . IMAGEPATH .$row->logo ?>">
                            <h5><?=$row->name?></h5>
                        </div>
                    </div>
                <?php endforeach ?>
            <?php else: ?>
                <div class="col-md-20 col-sm-3 col-xs-6 text-center fadeInUp wow" data-wow-delay=".2s"
                     data-wow-duration="1s">
                    <div class="values-box">
                        <img src="<?= base_url() . WEBASSETS ?>img/icons/thqa.png">
                        <h5>ان نكسب ثقة شركائنا</h5>
                    </div>
                </div>
                <div class="col-md-20 col-sm-3 col-xs-6 text-center fadeInUp wow" data-wow-delay=".4s"
                     data-wow-duration="1s">
                    <div class="values-box">
                        <img src="<?= base_url() . WEBASSETS ?>img/icons/office.png">
                        <h5>تكوين افضل بيئة عمل</h5>
                    </div>
                </div>
                <div class="col-md-20 col-sm-3 col-xs-6 text-center fadeInUp wow" data-wow-delay=".6s"
                     data-wow-duration="1s">
                    <div class="values-box">
                        <img src="<?= base_url() . WEBASSETS ?>img/icons/search.png">
                        <h5>استقطاب الكفاءات والمبدعين</h5>
                    </div>
                </div>
                <div class="col-md-20 col-sm-3 col-xs-6 text-center fadeInUp wow" data-wow-delay=".8s"
                     data-wow-duration="1s">
                    <div class="values-box">
                        <img src="<?= base_url() . WEBASSETS ?>img/icons/hands.png">
                        <h5>ان تكون افعالنا ابلغ من اقوالنا </h5>
                    </div>
                </div>
                <div class="col-md-20  col-sm-3 col-xs-6 text-center fadeInUp wow" data-wow-delay="1s"
                     data-wow-duration="1s">
                    <div class="values-box">
                        <img src="<?= base_url() . WEBASSETS ?>img/icons/shahda.png">
                        <h5>ان نكون العلامة التجارية الأولى في تخصصنا </h5>
                    </div>
                </div>
            <?php endif; ?>

        </div>
    </div>

</section>


<section class="features vision ptop-30 pbottom-40">
    <div class="section-header container mbottom- fadeInUp wow">
        <div class="container-fluid">
            <?php if (isset($spacial) && !empty($spacial)):
                $spacial_title = $spacial->name;
                $spacial_detals = $spacial->details;
                $spacial_img = base_url() . IMAGEPATH . $spacial->logo;
            else:
                $spacial_title = "تميزنا فى";
                $spacial_detals = 'نؤمن بأن التميز صناعة, لذا نلتزم بتحقيق أعلى معايير الجودة التي تمكّن شركائنا من صناعة النجاح في أعمالهم، ونسعى دائما أن نجمع شركائنا تحت مظلة واحدة من العلاقات التكاملية، التي تحقق استغلال الفرص المتاحة للاستثمار فيها، لبناء منظمات ومنشآت وطنية أكثر إحترافية تكون قادرة على المنافسة في ظل العولمة والمنافسة الإحترافية.';
                $spacial_img = base_url() . WEBASSETS . "img/icons/aim.png";
            endif;
            ?>

            <h3><?=$spacial_title?> :</h3>
            <p class="text-center"><?=$spacial_detals?></p><br>
        </div>
    </div>

    <div class="container-fluid">
        <?php if (isset($spacial->subs) && !empty($spacial->subs)): ?>
            <?php foreach ($spacial->subs as $row): ?>
                <div class="col-md-6 col-sm-6 col-xs-12 fadeInUp wow" data-wow-delay=".3s">
                    <div class="more-about ">
                        <img src="<?= base_url() . IMAGEPATH .$row->logo?>">
                        <div class="clrbg-before">
                            <h2 class="title-1"><?=$row->name?>: </h2>
                            <div class="pad-10"></div>
                            <p><?=$row->details?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        <?php else: ?>
            <div class="col-md-6 col-sm-6 col-xs-12 fadeInUp wow" data-wow-delay=".3s">
                <div class="more-about ">
                    <img src="<?= base_url() . WEBASSETS ?>img/icons/vision.png">
                    <div class="clrbg-before">
                        <h2 class="title-1">منطلقنا رؤية 2030: </h2>
                        <div class="pad-10"></div>
                        <p> انطلقنا من فهمنا العميقٍ للصورة الشاملة لتوجهات المملكة والتغيرات الاقتصادية والمبادرات
                            والتشريعات المتسارعة. </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 fadeInUp wow" data-wow-delay=".8s" data-wow-duration="1s">
                <div class="more-about ">
                    <img src="<?= base_url() . WEBASSETS ?>img/icons/message.png">
                    <div class="clrbg-before">
                        <h2 class="title-1">استشرافُنا المستقبل :</h2>
                        <div class="pad-10"></div>
                        <p> فضلًا عن التزام مجموعة المتسابق السعودي القابضة بإحاطة شركاءها دائما بما يستجد من المتطلبات
                            الحالية، فإننا ننظر إلى ما هو قادم من سياسات وتنظيمات وتغيراتٍ في الاقتصاد ونأخذها بالحسبان
                            في تصميم منتجاتنا.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 fadeInUp wow" data-wow-delay=".3s">
                <div class="more-about ">
                    <img src="<?= base_url() . WEBASSETS ?>img/icons/vision.png">
                    <div class="clrbg-before">
                        <h2 class="title-1">تخصصنا وريادتنا: </h2>
                        <div class="pad-10"></div>
                        <p> مجموعة المتسابق السعودي القابضة تعد من أوائل الشركات السعودية التي انطلقت بخدمات متعددة
                            ومستشارين متخصصين لتقديم الحلول المبتكرة والنوعية</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 fadeInUp wow" data-wow-delay=".8s" data-wow-duration="1s">
                <div class="more-about ">
                    <img src="<?= base_url() . WEBASSETS ?>img/icons/message.png">
                    <div class="clrbg-before">
                        <h2 class="title-1">خبرتنا في بيئات عمل متنوعة :</h2>
                        <div class="pad-10"></div>
                        <p> تكمن خبرتنا في الدراية والقدرة على التطبيق والموائمة حسب القطاع والاحتياج المخصص لكل
                            عميل.</p>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>

