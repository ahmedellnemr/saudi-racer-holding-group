<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>أخبار المتسابق السعودي ( المركز الإعلامي )</h1>

                <div class="divider-15 d-none d-xl-block"></div>
            </div>
        </div>
    </div>
</section>


<section class="main_content pbottom-30 ptop-10">
    <div class="container">
        <div class="news-details-page ">
            <div class="col-md-9 col-sm-8 col-xs-12 no-pxs">
                <div class="news-details-content">
                    <h5 class="title">

                        <a href="<?=base_url()."single-news?id=".$one->id?>"><?=$one->title?> </a>
                    </h5>
                    <p class="date"><i class="fa fa-calendar"></i><?=date("Y-m-d",$one->date)?>
                        <i class="fa fa-user fa-1x"></i> الناشر :
                        <?=(isset($one->user->name))? $one->user->name:"admin"?> </p>

                    <hr>
                    <div class="text-center">
                        <img src="<?=base_url().IMAGEPATH. $one->logo ?>" class="main-img">
                    </div>

                    <br>
                    <hr class="white">
                    <p class="paragraph"><?=$one->details?> </p>
                    <?php if (isset($one->subs) && !empty($one->subs)): ?>
                    <p class="publisher">صور من الموضوع</p>
                    <ul class="clearfix list-unstyled related-imgs " id="thumbnails">
                            <?php foreach ($one->subs as $row ): ?>
                                <li class="mbottom-20">
                                    <div class="box-img">
                                        <a href="<?= base_url() . IMAGEPATH .$row->name?>"
                                           title="">
                                            <img src="<?= base_url() . IMAGEPATH .$row->name?>"
                                                 alt="" width="100%"
                                                 height="250">
                                        </a>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                    </ul>
                    <?php endif ?>
                    <hr>
                    <?php if (!empty($one->vedio_link)): ?>
                        <div class="news-videos">
                            <h5 class="text-center">فيديو الخبر</h5>
                            <div>
                                <iframe width="100%" height="415" src="https://www.youtube.com/embed/<?=$one->vedio_link?>"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>

                            </div>
                        </div>
                    <?php endif ?>

                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12 no-padding">

                <div class="sidebar">
                    <h5 class="heading">أخبار مختارة</h5>
                    <div class="related-news">
                        <ul class="list-unstyled">

                            <?php if (isset($news) && !empty($news) ): ?>
                                <?php foreach ($news as $row): ?>
                                    <li class="single">
                                        <a href="<?=base_url()."single-news?id=".$row->id?>">
                                            <img src="<?= base_url() . IMAGEPATH . $row->logo ?>" width="80"
                                                 height="80">
                                            <h6><?=$row->title?> </h6>
                                        </a>
                                    </li>
                                <?php endforeach ?>
                            <?php endif ?>


                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>



