 
<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>( <?=$one->name?> )</h1>

                <div class="divider-15 d-none d-xl-block"></div>
            </div>
        </div>
    </div>
</section>


<section class="main_content pbottom-30 ptop-10">
    <div class="container">

        <div class="col-md-3 col-sm-3">
            <div class="text-center">
              <a style="cursor: pointer;" data-toggle="modal" data-target="#myModal"><img src="<?=base_url().IMAGEPATH. $one->logo ?>" class="team-member-img"></a>  
          </div>
      </div>
      <div class="col-md-9 col-sm-9">
        <div class="team-member-details">
            <h2><?=$one->name?></h2>
            <h3><?=$one->job_title?></h3>
            <p class="pargraph"><?=$one->details?></p>
        </div>
    </div>
    

</div>
</section>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?=$one->name?></h4>
    </div>
    <div class="modal-body">
      <div class="text-center">
         <img src="<?=base_url().IMAGEPATH. $one->logo ?>" >
         <h3><?=$one->job_title?></h3>
     </div>
 </div>
 <div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal">إغلاق</button>
</div>
</div>
</div>
</div>