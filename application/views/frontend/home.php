<!------------------------------------------------------------------------------------------------------->
<section class="banner">

	<!-- Slider Revolution Start -->
	<div class="container-fluid no-padding">
		<div class="rev_slider_wrapper" style="direction: ltr">
			<div class="rev_slider" data-version="5.0" >
				<ul>
					<!-- SLIDE 1 -->
					<li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?=base_url().WEBASSETS?>img/rev/desk_new.png" data-rotate="0" data-saveperformance="off" data-title="تقنية المعلومات و التطبيقات" data-description="">
						<!-- MAIN IMAGE -->
						<img src="<?=base_url().WEBASSETS?>img/rev/lightbg.png"  alt=""  data-bgposition="center center" data-bgfit="100% 100%" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>


							<!-- LAYER NR. 1 -->
						<div class="tp-caption tp-resizeme text-uppercase  bg-dark-transparent  skewfromleft  pleft-30 pright-30" 
							id="rs-1-layer-1"
							data-x="['center']"
							data-hoffset="['0']"
							data-y="['top']"
							data-voffset="['20']" 
							data-fontsize="['28']"

							data-lineheight="['54']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-transform_idle="o:1;s:1000"
							data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
							data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
							data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
							data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
							data-start="1000" 
							data-splitin="none" 
							data-splitout="none" 
							data-responsive_offset="on"
							style="z-index: 1; white-space: nowrap; font-weight:400; border-radius: 30px;">

							<h5 class="hl" style="font-size:22px">تقنية المعلومات و التطبيقات</h5>
					   </div>

					   	<!-- LAYER NR. 1 -->
						<div class="tp-caption tp-resizeme text-uppercase randomrotate bg-dark-transparent " 
							id="rs-1-layer-2"
							data-x="['center']"
							data-hoffset="['0']"
							data-y="['top']"
							data-voffset="['60']" 
							data-fontsize="['28']"

							data-lineheight="['54']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-transform_idle="o:1;s:1500"
							data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
							data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
							data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
							data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
							data-start="1500" 
							data-splitin="none" 
							data-splitout="none" 
							data-responsive_offset="on"
							style="z-index: 1; white-space: nowrap; font-weight:400; border-radius: 30px;">

							<img src="<?=base_url().WEBASSETS?>img/rev/it-icons.png"  class="slider-icon">
					   </div>

					   




					   <!-- LAYER NR. 1 -->
						<div class="tp-caption tp-resizeme text-uppercase lfb  bg-dark-transparent " 
						id="rs-1-layer-3"
						data-x="['center']"
						data-hoffset="['0']"
						data-y="['bottom']"
						data-voffset="['0']" 
						data-fontsize="['28']"
						data-lineheight="['54']"
						data-width="none"
						data-height="none"
						data-whitespace="nowrap"
						data-transform_idle="o:1;s:1000"
						data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
						data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
						data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
						data-start="1000" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on"
						style="z-index: 7; white-space: nowrap; font-weight:400; border-radius: 30px;">
						<img src="<?=base_url().WEBASSETS?>img/rev/desk_new.png"> 
					   </div>

					   <!-- LAYER NR. 1 -->
						<div class="tp-caption tp-resizeme text-uppercase sfr  bg-dark-transparent " 
						id="rs-1-layer-4"
						data-x="['right']"
						data-hoffset="['0']"
						data-y="['bottom']"
						data-voffset="['0']" 
						data-fontsize="['28']"
						data-lineheight="['54']"
						data-width="none"
						data-height="none"
						data-whitespace="nowrap"
						data-transform_idle="o:1;s:3000"
						data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
						data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
						data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
						data-start="1500" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on"
						style="z-index: 7; white-space: nowrap; font-weight:400; border-radius: 30px;">
						<img src="<?=base_url().WEBASSETS?>img/rev/emirati2.png"> 
					   </div>


					   <!-- LAYER NR. 1 -->
						<div class="tp-caption tp-resizeme text-uppercase randomrotate bg-dark-transparent " 
						id="rs-1-layer-5"
						data-x="['center']"
						data-hoffset="['50']"
						data-y="['middle']"
						data-voffset="['0']" 
						data-fontsize="['28']"
						data-lineheight="['54']"
						data-width="none"
						data-height="none"
						data-whitespace="nowrap"
						data-transform_idle="o:1;s:1000"
						data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
						data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
						data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
						data-start="2000" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on"
						style="z-index: 1; white-space: nowrap; font-weight:400; border-radius: 30px;">
						<img src="<?=base_url().WEBASSETS?>img/rev/examplebar3.jpg"> 
					   </div>



					</li>


                     <!-- SLIDE 2 -->
					<li data-index="rs-2" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?=base_url().WEBASSETS?>img/rev/brochure-design.png" data-rotate="0" data-saveperformance="off" data-title="الدعاية والاعلان والتسويق" data-description="">
						<!-- MAIN IMAGE -->
						<img src="img/lightbg.png"  alt=""  data-bgposition="center center" data-bgfit="100% 100%" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>


							<!-- LAYER NR. 1 -->
						<div class="tp-caption tp-resizeme text-uppercase  bg-dark-transparent  skewfromleft  pleft-30 pright-30" 
							id="rs-2-layer-1"
							data-x="['center']"
							data-hoffset="['0']"
							data-y="['top']"
							data-voffset="['20']" 
							data-fontsize="['28']"

							data-lineheight="['54']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-transform_idle="o:1;s:1000"
							data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
							data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
							data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
							data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
							data-start="1000" 
							data-splitin="none" 
							data-splitout="none" 
							data-responsive_offset="on"
							style="z-index: 1; white-space: nowrap; font-weight:400; border-radius: 30px;">

							<h5 class="hl" style="font-size:22px">الدعاية والاعلان والتسويق</h5>
					   </div>

					   	<!-- LAYER NR. 1 -->
						<div class="tp-caption tp-resizeme text-uppercase lfb  bg-dark-transparent " 
							id="rs-2-layer-2"
							data-x="['center']"
							data-hoffset="['0']"
							data-y="['top']"
							data-voffset="['90']" 
							data-fontsize="['28']"

							data-lineheight="['54']"
							data-width="none"
							data-height="none"
							data-whitespace="nowrap"
							data-transform_idle="o:1;s:1500"
							data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
							data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
							data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
							data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
							data-start="1500" 
							data-splitin="none" 
							data-splitout="none" 
							data-responsive_offset="on"
							style="z-index: 1; white-space: nowrap; font-weight:400; border-radius: 30px;">

							<img src="<?=base_url().WEBASSETS?>img/rev/hero-bigboard.png" class="amoud-pic" >
					   </div>

					   


					   <!-- LAYER NR. 1 -->
						<div class="tp-caption tp-resizeme text-uppercase randomrotate bg-dark-transparent " 
						id="rs-2-layer-3"
						data-x="['right']"
						data-hoffset="['50']"
						data-y="['middle']"
						data-voffset="['0']" 
						data-fontsize="['28']"
						data-lineheight="['54']"
						data-width="none"
						data-height="none"
						data-whitespace="nowrap"
						data-transform_idle="o:1;s:1000"
						data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
						data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
						data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
						data-start="2000" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on"
						style="z-index: 1; white-space: nowrap; font-weight:400; border-radius: 30px;">
						<img src="<?=base_url().WEBASSETS?>img/rev/brochure-design.png" > 
					   </div>

					   <!-- LAYER NR. 1 -->
						<div class="tp-caption tp-resizeme text-uppercase randomrotate bg-dark-transparent " 
						id="rs-2-layer-4"
						data-x="['left']"
						data-hoffset="['50']"
						data-y="['middle']"
						data-voffset="['0']" 
						data-fontsize="['28']"
						data-lineheight="['54']"
						data-width="none"
						data-height="none"
						data-whitespace="nowrap"
						data-transform_idle="o:1;s:1000"
						data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
						data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
						data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
						data-start="2500" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on"
						style="z-index: 1; white-space: nowrap; font-weight:400; border-radius: 30px;">
						<img src="<?=base_url().WEBASSETS?>img/rev/business-card-png-4.png" > 
					   </div>




					</li>








					<!-- SLIDE 2 -->
					<li data-index="rs-3" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?=base_url().WEBASSETS?>img/rev/rally-car.png" data-rotate="0" data-saveperformance="off" data-title="عنوان الصورة الثانية 2" data-description="">
						<!-- MAIN IMAGE -->
						<img src="<?=base_url().WEBASSETS?>img/rev/rally-car.png"  alt=""  data-bgposition="center center" data-bgfit="100% 100%" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>
      
 
                       <!-- LAYER NR. 1 -->
						<div class="tp-caption tp-resizeme text-uppercase sfr  bg-dark-transparent " 
						id="rs-1-layer-4"
						data-x="['right']"
						data-hoffset="['0']"
						data-y="['bottom']"
						data-voffset="['0']" 
						data-fontsize="['28']"
						data-lineheight="['54']"
						data-width="none"
						data-height="none"
						data-whitespace="nowrap"
						data-transform_idle="o:1;s:4000"
						data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
						data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
						data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
						data-start="1500" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on"
						style="z-index: 7; white-space: nowrap; font-weight:400; border-radius: 30px;">
						<img src="<?=base_url().WEBASSETS?>img/rev/default-driver.png" class="default-driver"> 
					   </div>
                       
                       
                       
                       <!-- LAYER NR. 2 
						<div class="tp-caption tp-resizeme text-uppercase sfr  bg-dark-transparent " 
						id="rs-1-layer-4"
						data-x="['left']"
						data-hoffset="['0']"
						data-y="['bottom']"
						data-voffset="['0']" 
						data-fontsize="['28']"
						data-lineheight="['54']"
						data-width="none"
						data-height="none"
						data-whitespace="nowrap"
						data-transform_idle="o:1;s:1000"
						data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
						data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
						data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
						data-start="1500" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on"
						style="z-index: 7; white-space: nowrap; font-weight:400; border-radius: 30px;">
						<img src="<?=base_url().WEBASSETS?>img/rev/doctor-ahmed.png" class="default-driver"> 
					   </div>
                       -->
					 
					</li>
                    
                    
            
            
                  <?php if (isset($sliders) && $sliders!= null && !empty($sliders)): ?>
                  <?php $x=4;
                      
                        foreach ($sliders as $row): ?>
                    <li data-index="rs-<?php echo $x;?>" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?php echo IMAGEPATH.$row->logo;?>" data-rotate="0" data-saveperformance="off" data-title="عنوان الصورة الثانية 2" data-description="">
						<!-- MAIN IMAGE -->
						<img src="<?php echo IMAGEPATH.$row->logo;?>"  alt=""  data-bgposition="center center" data-bgfit="100% 100%" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>

						 
					</li>
                    
                     <?php $x++; endforeach; ?>
                     <?php endif ?>
					

            	<li class="hidden" data-index="rs-65" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="img/slider/slide-4.jpg" data-rotate="0" data-saveperformance="off" data-title="عنوان الصورة الثالثة 3" data-description="">
            		
            		<img src="<?=base_url().WEBASSETS?>img/rev/business-card-png-4.png"  alt=""  data-bgposition="center center" data-bgfit="100% 100%" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina >
            	</li>

</ul>
</div><!-- end .rev_slider -->
</div>
</div>



















<!--
    <div id="main-slider" class="carousel slide carousel-fade" data-ride="carousel">
        
        <ol class="carousel-indicators">
            <?php if (isset($sliders) && $sliders!= null && !empty($sliders)): ?>
                <?php $x=0;
                $htmlSlider = '';
                foreach ($sliders as $row):
                    $act  = ($x == 0)? "active":"";
                    $htmlSlider .= ' <div class="item '.$act.'">
                                        <div class="bg-slide">
                                            <img src="'.IMAGEPATH.$row->logo.'" alt="صورة">
                                        </div>
                                        <div class="carousel-caption">
                                            ...
                                        </div>
                                    </div>';
                    ?>
                    <li data-target="#main-slider" data-slide-to="<?=$x?>" class="<?=$act?>"></li>
                <?php $x++; endforeach; ?>
            <?php else: ?>
                <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                <li data-target="#main-slider" data-slide-to="1"></li>
                <li data-target="#main-slider" data-slide-to="2"></li>
            <?php endif ?>

        </ol>

       
        <div class="carousel-inner" role="listbox">
            <?php if (isset($sliders) && $sliders!= null && !empty($sliders)): ?>
            <?= $htmlSlider?>
            <?php else: ?>
            <div class="item active">
                <div class="bg-slide">
                    <img src="<?=base_url().WEBASSETS?>img/banner/img2.png" alt="الرياض">
                </div>
                <div class="carousel-caption">
                    ...
                </div>
            </div>
            <div class="item">
                <div class="bg-slide">
                    <img src="<?=base_url().WEBASSETS?>img/banner/img1.png" alt="الرياض">
                </div>
                <div class="carousel-caption">
                    ...
                </div>
            </div>
            <div class="item">
                <div class="bg-slide">
                    <img src="<?=base_url().WEBASSETS?>img/banner/img1.png" alt="الرياض">
                </div>
                <div class="carousel-caption">
                    ...
                </div>
            </div>
            <?php endif ?>
        </div>

      
        <a class="left carousel-control" href="#main-slider" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#main-slider" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>-->

</section>
<!------------------------------------------------------------------------------------------------------->
<section class="sec-benefits">
    <div class="section-header container">
        <div class="container-fluid">
            <h3>فروع المجموعة</h3>
        </div>
    </div>

    <div id="Benefits-Section">
        <div class="container-fluid">
            <div class="col-xs-12 no-padding text-center spaceAfterBeforTitleLine">

                <?php if (isset($branches) && $branches!= null && !empty($branches)): ?>
                <?php foreach ($branches as $row): ?>
                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-xs-100 LightBlocksPanel BlueTheme wow fadeInUp " data-wow-delay=".3s">
                        <div class="panel-heading"></div>
                        <div class="panel LightBlocks-Data popover-wrapper">
                            <div class="BenefitsImage  Benefits-1">
                                <a href="<?=base_url()."single-branch?id=".$row->id?>">
                                    <img src="<?=base_url().IMAGEPATH.$row->icon?>">
                                </a>
                            </div>
                            <p class="LightBlocks-Description"><?=$row->name?></p>
                            <div class=" popover-box ">
                                <h3 class="popover-title"><?=$row->name?></h3>
                                <p><?=word_limiter($row->details,45)?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
                <?php else: ?>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-xs-100 LightBlocksPanel BlueTheme wow fadeInUp " data-wow-delay=".3s">
                    <div class="panel-heading"></div>
                    <div class="panel LightBlocks-Data popover-wrapper">
                        <div class="BenefitsImage  Benefits-1">
                            <a href="#"><img src="<?=base_url().WEBASSETS?>img/icons/Download-Advertising-PNG-File.png"></a>
                        </div>
                        <p class="LightBlocks-Description">الدعاية والاعلان والتسويق</p>
                        <div class=" popover-box ">
                            <h3 class="popover-title">الدعاية والاعلان والتسويق</h3>
                            <p>بمفهوم عصري وعالمي من خلال الخبرات الواسعة وعبر نماذج فريدة نعمل على تقديم ما يتناسب من طموحات شركائنا ضمن رسالة واقعية واضحة تساعد على فهم الإعلان من قبل المتلقي من خلال منظور دراسات متخصصة في الحلول الاعلانية</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-xs-100 LightBlocksPanel GreenTheme wow fadeInUp" data-wow-delay=".4s">
                    <div class="panel-heading"></div>
                    <div class="panel LightBlocks-Data popover-wrapper">
                        <div class="BenefitsImage  Benefits-2">
                            <a href="#"><img src="<?=base_url().WEBASSETS?>img/icons/car.png"></a>
                        </div>
                        <p class="LightBlocks-Description">الفريق الأول لرياضة السيارات</p>
                        <div class=" popover-box ">
                            <h3 class="popover-title">الفريق الأول لرياضة السيارات</h3>
                            <p>لضرورة واهمية وجود فريق لرياضة السيارات سعودي يهدف الى تأهيل ورعاية الشباب السعودي الشغوف بهذه الرياضة أنشئ الفريق ليشمل جميع أنواع رياضة السيارات</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-xs-100 LightBlocksPanel MaroonTheme wow fadeInUp" data-wow-delay=".5s">
                    <div class="panel-heading"></div>
                    <div class="panel LightBlocks-Data popover-wrapper">
                        <div class="BenefitsImage  Benefits-3">
                            <a href="#"><img src="<?=base_url().WEBASSETS?>img/icons/services.png"></a>
                        </div>
                        <p class="LightBlocks-Description">صيانة وتعديل السيارات الرياضية</p>
                        <div class=" popover-box ">
                            <h3 class="popover-title">صيانة وتعديل السيارات الرياضية</h3>
                            <p>استمرارا لدعم رياضة السيارات أنشئ المركز ليحتضن هذه الرياضة وايضا ليقدم خدمات الصيانة وتعديل السيارات بشتّى أنواعها وليكون الدليل والمرجع للشباب السعودي الشغوف بهذه الرياضة </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-xs-100 LightBlocksPanel BurpleTheme wow fadeInUp" data-wow-delay=".3s">
                    <div class="panel-heading"></div>
                    <div class="panel LightBlocks-Data popover-wrapper">
                        <div class="BenefitsImage  Benefits-4">
                            <a href="#"><img src="<?=base_url().WEBASSETS?>img/icons/it-icon-png-7.jpg"></a>
                        </div>
                        <p class="LightBlocks-Description">تقنية المعلومات</p>
                        <div class=" popover-box ">
                            <h3 class="popover-title">تقنية المعلومات</h3>
                            <p>نقدم حلول تقنية متكاملة في بناء وادارة الانظمة التقنية لاننا نعتقد أن التقنية يجب ان تطوّع لخدمة المنشآت ونعمل على تبسيط الأدوات التقنية لجعلها قابلة للاستخدام بيسر وسهولة لكي نمكّن المنشآت من التركيز على عملها بدلا من تضييع الوقت في التعقيدات التقنية</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-xs-100 LightBlocksPanel RedTheme wow fadeInUp" data-wow-delay=".4s">
                    <div class="panel-heading"></div>
                    <div class="panel LightBlocks-Data popover-wrapper">
                        <div class="BenefitsImage  Benefits-5">
                            <a href="#"><img src="<?=base_url().WEBASSETS?>img/icons/Conference-icon.png"></a>
                        </div>
                        <p class="LightBlocks-Description">تنظيم المعارض والمؤتمرات</p>
                        <div class=" popover-box ">
                            <h3 class="popover-title">تنظيم المعارض والمؤتمرات</h3>
                            <p>نعمل من خلال منظومة متكاملة على انشاء وتنفيذ وتسويق المعارض والمؤتمرات والاشراف عليها أيضا اخذين بعين الاعتبار افضل الممارسات العلمية والعملية لذلك , نسعى لتمكين شركائنا بتدريبهم وتطويرهم من خلال خدماتنا المتميزة اخذين بعين الاعتبار افضل الممارسات المحلية والعالمية بدءا من التفكير والتصميم ومن ثم التنفيذ وتقييم الأثر والنتائج من اجل التحسين </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-xs-100 LightBlocksPanel PinkTheme wow fadeInUp" data-wow-delay=".5s">
                    <div class="panel-heading"></div>
                    <div class="panel LightBlocks-Data popover-wrapper">
                        <div class="BenefitsImage  Benefits-6">
                            <a href="#"><img src="<?=base_url().WEBASSETS?>img/icons/site-setting.png"></a>
                        </div>
                        <p class="LightBlocks-Description">الصيانة والتشغيل</p>
                        <div class=" popover-box ">
                            <h3 class="popover-title">الصيانة والتشغيل</h3>
                            <p>نعمل من خلال منظومة متكاملة نوفر من خلالها لشركائنا كل ما يلزم في مجال الصيانة والتشغيل ورسم خطة عمل كاملة وتنفيذها كذلك بالإضافة الى خبرات مستشارينا الواسعة نقدم خدمات استشارية فيه هذا الجانب تساعد على تقييم الوضع الراهن وافضل الممارسات العلمية والعملية التي تنهض بالنشاطات والشركات المتعثرة</p>
                        </div>
                        <p></p>
                    </div>
                </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</section>
<!------------------------------------------------------------------------------------------------------->
<section class="our-team  wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
    <div class="section-header container">
        <div class="container-fluid">
            <h3>فريق ماهر يعمل لدينا</h3>
        </div>
    </div>
    <div class="container-fluid">
        <div id="owl-team" class="owl-carousel owl-theme">

            <?php if (isset($team) && $team!= null && !empty($team)): ?>
            <?php foreach ($team as $row): ?>
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="profile-image">
                                        <img class=" img-fluid" src="<?=base_url().IMAGEPATH.$row->logo?>" alt="card image">
                                    </div>
                                    <h4 class="card-title"><?=$row->name?> </h4>
                                    <p class="card-text"><?=$row->job_title?></p>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title"><?=$row->name?> </h4>
                                    <p class="card-text"><?=word_limiter($row->details,40)?></p>
                                    <a href="<?=base_url()."Web/single_team?id=".$row->id?>" class="btn btn-success"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
              <?php endforeach;?>
                <?php else:?>
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="profile-image">
                                        <img class=" img-fluid" src="<?=base_url().WEBASSETS?>img/team/mohamed_almalki.png" alt="card image">
                                    </div>
                                    <h4 class="card-title">د. محمد بن احمد المالكي </h4>
                                    <p class="card-text">رئيس مجلس الادارة</p>

                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">د. محمد بن احمد المالكي </h4>
                                    <p class="card-text">دكتوراه تسويق
                                        مؤسس مجموعة المتسابق السعودي القابضة
                                        مؤسس الفريق السعودي الأول لرياضة السيارات
                                        حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات
                                        عضو مجلس إدارات عدد من الشركات المحلية والعالمية
                                        شارك في تأسيس العديد من الشركات المحلية والعالمية
                                        مستشارا لعدد من مجالس الإدارات
                                        كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات

                                    </p>


                                    <a href="#" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="profile-image">
                                        <img class=" img-fluid" src="<?=base_url().WEBASSETS?>img/team/fahad_almalki.png" alt="card image">
                                    </div>
                                    <h4 class="card-title">أ. فهد محمد المالكي </h4>
                                    <p class="card-text">نائب رئيس مجلس الادارة</p>

                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">أ. فهد محمد المالكي </h4>
                                    <p class="card-text">دكتوراه تسويق
                                        مؤسس مجموعة المتسابق السعودي القابضة
                                        مؤسس الفريق السعودي الأول لرياضة السيارات
                                        حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات
                                        عضو مجلس إدارات عدد من الشركات المحلية والعالمية
                                        شارك في تأسيس العديد من الشركات المحلية والعالمية
                                        مستشارا لعدد من مجالس الإدارات
                                        كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات

                                    </p>


                                    <a href="#" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="profile-image">
                                        <img class=" img-fluid" src="<?=base_url().WEBASSETS?>img/team/ahmed_bawazir.png" alt="card image">
                                    </div>
                                    <h4 class="card-title">أ. احمد باوزير </h4>
                                    <p class="card-text">المشرف العام</p>

                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">أ. احمد باوزير</h4>
                                    <p class="card-text">دكتوراه تسويق
                                        مؤسس مجموعة المتسابق السعودي القابضة
                                        مؤسس الفريق السعودي الأول لرياضة السيارات
                                        حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات
                                        عضو مجلس إدارات عدد من الشركات المحلية والعالمية
                                        شارك في تأسيس العديد من الشركات المحلية والعالمية
                                        مستشارا لعدد من مجالس الإدارات
                                        كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات

                                    </p>


                                    <a href="#" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="profile-image">
                                        <img class=" img-fluid" src="<?=base_url().WEBASSETS?>img/team/ahmed_almalki.png" alt="card image">
                                    </div>
                                    <h4 class="card-title">م. احمد الحصيبي </h4>
                                    <p class="card-text">إدارة الاستثمار</p>

                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">م. احمد الحصيبي </h4>
                                    <p class="card-text">دكتوراه تسويق
                                        مؤسس مجموعة المتسابق السعودي القابضة
                                        مؤسس الفريق السعودي الأول لرياضة السيارات
                                        حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات
                                        عضو مجلس إدارات عدد من الشركات المحلية والعالمية
                                        شارك في تأسيس العديد من الشركات المحلية والعالمية
                                        مستشارا لعدد من مجالس الإدارات
                                        كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات

                                    </p>


                                    <a href="#" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="profile-image">
                                        <img class=" img-fluid" src="<?=base_url().WEBASSETS?>img/team/abdallaha_almalki.png" alt="card image">
                                    </div>
                                    <h4 class="card-title">أ. عبدالله الحصيبي</h4>
                                    <p class="card-text">الإدارة الفنية</p>

                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">أ. عبدالله الحصيبي </h4>
                                    <p class="card-text">دكتوراه تسويق
                                        مؤسس مجموعة المتسابق السعودي القابضة
                                        مؤسس الفريق السعودي الأول لرياضة السيارات
                                        حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات
                                        عضو مجلس إدارات عدد من الشركات المحلية والعالمية
                                        شارك في تأسيس العديد من الشركات المحلية والعالمية
                                        مستشارا لعدد من مجالس الإدارات
                                        كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات

                                    </p>


                                    <a href="#" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="profile-image">
                                        <img class=" img-fluid" src="<?=base_url().WEBASSETS?>img/team/1.png" alt="card image">
                                    </div>
                                    <h4 class="card-title">أ. محمود عبدالعال</h4>
                                    <p class="card-text">الإدارة المالية</p>

                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">أ. محمود عبدالعال </h4>
                                    <p class="card-text">دكتوراه تسويق
                                        مؤسس مجموعة المتسابق السعودي القابضة
                                        مؤسس الفريق السعودي الأول لرياضة السيارات
                                        حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات
                                        عضو مجلس إدارات عدد من الشركات المحلية والعالمية
                                        شارك في تأسيس العديد من الشركات المحلية والعالمية
                                        مستشارا لعدد من مجالس الإدارات
                                        كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات

                                    </p>


                                    <a href="#" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="profile-image">
                                        <img class=" img-fluid" src="<?=base_url().WEBASSETS?>img/team/2.png" alt="card image">
                                    </div>
                                    <h4 class="card-title">أ. طاهر بخش </h4>
                                    <p class="card-text">الإدارة الاعلامية</p>

                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">أ. طاهر بخش </h4>
                                    <p class="card-text">دكتوراه تسويق
                                        مؤسس مجموعة المتسابق السعودي القابضة
                                        مؤسس الفريق السعودي الأول لرياضة السيارات
                                        حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات
                                        عضو مجلس إدارات عدد من الشركات المحلية والعالمية
                                        شارك في تأسيس العديد من الشركات المحلية والعالمية
                                        مستشارا لعدد من مجالس الإدارات
                                        كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات

                                    </p>


                                    <a href="#" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="profile-image">
                                        <img class=" img-fluid" src="<?=base_url().WEBASSETS?>img/team/3.png" alt="card image">
                                    </div>
                                    <h4 class="card-title">أ. طلال بخش</h4>
                                    <p class="card-text">إدارة التطوير والجودة</p>

                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">أ. طلال بخش</h4>
                                    <p class="card-text">دكتوراه تسويق
                                        مؤسس مجموعة المتسابق السعودي القابضة
                                        مؤسس الفريق السعودي الأول لرياضة السيارات
                                        حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات
                                        عضو مجلس إدارات عدد من الشركات المحلية والعالمية
                                        شارك في تأسيس العديد من الشركات المحلية والعالمية
                                        مستشارا لعدد من مجالس الإدارات
                                        كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات

                                    </p>


                                    <a href="#" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="profile-image">
                                        <img class=" img-fluid" src="<?=base_url().WEBASSETS?>img/team/4.png" alt="card image">
                                    </div>
                                    <h4 class="card-title">أ. احمد رضوان </h4>
                                    <p class="card-text">إدارة التسويق</p>

                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">أ. احمد رضوان </h4>
                                    <p class="card-text">دكتوراه تسويق
                                        مؤسس مجموعة المتسابق السعودي القابضة
                                        مؤسس الفريق السعودي الأول لرياضة السيارات
                                        حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات
                                        عضو مجلس إدارات عدد من الشركات المحلية والعالمية
                                        شارك في تأسيس العديد من الشركات المحلية والعالمية
                                        مستشارا لعدد من مجالس الإدارات
                                        كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات

                                    </p>


                                    <a href="#" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="profile-image">
                                        <img class=" img-fluid" src="<?=base_url().WEBASSETS?>img/team/5.png" alt="card image">
                                    </div>
                                    <h4 class="card-title">أ. هشام محروس</h4>
                                    <p class="card-text">إدارة المبيعات</p>

                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">أ. هشام محروس</h4>
                                    <p class="card-text">دكتوراه تسويق
                                        مؤسس مجموعة المتسابق السعودي القابضة
                                        مؤسس الفريق السعودي الأول لرياضة السيارات
                                        حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات
                                        عضو مجلس إدارات عدد من الشركات المحلية والعالمية
                                        شارك في تأسيس العديد من الشركات المحلية والعالمية
                                        مستشارا لعدد من مجالس الإدارات
                                        كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات

                                    </p>


                                    <a href="#" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="profile-image">
                                        <img class=" img-fluid" src="<?=base_url().WEBASSETS?>img/team/6.png" alt="card image">
                                    </div>
                                    <h4 class="card-title">أ. عبدالله السلطان</h4>
                                    <p class="card-text">إدارة العلاقات العامة</p>

                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">أ. عبدالله السلطان</h4>
                                    <p class="card-text">دكتوراه تسويق
                                        مؤسس مجموعة المتسابق السعودي القابضة
                                        مؤسس الفريق السعودي الأول لرياضة السيارات
                                        حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات
                                        عضو مجلس إدارات عدد من الشركات المحلية والعالمية
                                        شارك في تأسيس العديد من الشركات المحلية والعالمية
                                        مستشارا لعدد من مجالس الإدارات
                                        كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات

                                    </p>


                                    <a href="#" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<!------------------------------------------------------------------------------------------------------->
<section class="contact-sec">
    <div class="container ptop-40 pbottom-50">
        <div class="section-content">
            <div class="col-xs-12 no-padding">

                <div class="col-md-7 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h3 class="title line-bottom">مميزات التعامل مع <span class="text-theme-color-2 ">المتسابق السعودي</span></h3>

                    <?php if (isset($features) && $features!= null && !empty($features)):  ?>
                    <?php $iconat = array("fa fa-music","fa fa-home","","");  ?>
                    <?php foreach ($features as $row): ?>
                    <div class="col-sm-12 col-md-12 ">
                        <div class="box-icon-2">
                            <div class="icon">
                            <img src="<?=base_url().IMAGEPATH.$row->logo?>" />
                                
                            </div>
                            <div class="body-content">
                                <h4><?=$row->name?></h4>
                                <p><?=$row->details?></p>
                            </div>
                        </div>
                    </div>
                        <?php endforeach;?>
                    <?php else:?>
                    <div class="col-sm-12 col-md-12 ">
                        <div class="box-icon-2">
                            <div class="icon">
                                <i class="fa fa-music"></i>
                            </div>
                            <div class="body-content">
                                <h4>منطلقنا رؤية 2030</h4>
                                <p class="">انطلقنا من فهمنا العميقٍ للصورة الشاملة لتوجهات المملكة والتغيرات الاقتصادية والمبادرات والتشريعات المتسارعة.  </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 ">
                        <div class="box-icon-2">
                            <div class="icon">
                                <i class="fa fa-music"></i>
                            </div>
                            <div class="body-content">
                                <h4>استشرافُنا المستقبل</h4>
                                <p class="">فضلًا عن التزام مجموعة المتسابق السعودي القابضة بإحاطة شركاءها دائما بما يستجد من المتطلبات الحالية، فإننا ننظر إلى ما هو قادم من سياسات وتنظيمات وتغيراتٍ في الاقتصاد ونأخذها بالحسبان في تصميم منتجاتنا.  </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 ">
                        <div class="box-icon-2">
                            <div class="icon">
                                <i class="fa fa-music"></i>
                            </div>
                            <div class="body-content">
                                <h4>تخصصنا وريادتنا</h4>
                                <p class="">مجموعة المتسابق السعودي القابضة تعد من أوائل الشركات السعودية التي انطلقت بخدمات متعددة ومستشارين متخصصين لتقديم الحلول المبتكرة والنوعية </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 ">
                        <div class="box-icon-2">
                            <div class="icon">
                                <i class="fa fa-music"></i>
                            </div>
                            <div class="body-content">
                                <h4>خبرتنا في بيئات عمل متنوعة</h4>
                                <p class="">تكمن خبرتنا في الدراية والقدرة على التطبيق والموائمة حسب القطاع والاحتياج المخصص لكل عميل. </p>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>


                </div>
                <div class="col-md-5 wow fadeInUp contact" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h3 class="head text-white">اترك رسالة لنا</h3>
                    <form id="" class="bg-light" name="" action="" >
                        <!--------------------------------------------------->
                        <div class="col-xs-12 no-padding">
                            <div class="col-sm-6" style="padding-right: 0;">
                                <div class="form-group">
                                    <label for="form_name">الإسم بالكامل <small>*</small></label>
                                    <input id="form_name" name="name" type="text" placeholder="ضع اسمك" required="" class="form-control send-contact">
                                </div>
                            </div>
                            <div class="col-sm-6" style="padding-left: 0;">
                                <div class="form-group">
                                    <label for="form_email">البريد الإلكترونى <small>*</small></label>
                                    <input id="form_email" name="email" class="form-control send-contact" type="email" placeholder="Ahmed@gmail.com">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 no-padding">
                            
                                <div class="form-group">
                                    <label for="form_name">الموضوع <small>*</small></label>
                                    <input id="form_name" name="subject" type="text" placeholder="الموضوع" required="" class="form-control send-contact">
                                </div>
                           
                        </div>
                        <div class="form-group">
                            <label for="form_message">الرسالة <small>*</small></label>
                            <textarea id="form_message" name="message" class="form-control send-contact" rows="5" placeholder="...."></textarea>
                        </div>
                        <div class="form-group">

                            <button type="button" class="btn btn-block btn-dark btn-theme-colored btn-sm mt-20 pt-10 pb-10 send-contact-btn" data-loading-text="Please wait...">أرسل الأن</button>
                        </div>
                        <!--------------------------------------------------->
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
<!------------------------------------------------------------------------------------------------------->
<section class="clients pbottom-50">
    <div class="container">
        <h2  class="text-center heading">شركاء النجاح <br> عملاء نعتز بخدمتهم</h2>
        <div class="container">
            <div id="owl-demo1" class="owl-carousel owl-theme">
                <?php if (isset($partners) && $partners!= null && !empty($partners)): ?>
                <?php foreach ($partners as $row): ?>
                <div class="item">
                    <img src="<?=base_url().IMAGEPATH.$row->logo?>" class="img-responsive " title="" />
                </div>
                    <?php endforeach;?>
                <?php else:?>
                <div class="item">
                    <img src="<?=base_url().WEBASSETS?>img/logos/img1.png" class="img-responsive " title="" />
                </div>
                <div class="item">
                    <img src="<?=base_url().WEBASSETS?>img/logos/img2.jpg" class="img-responsive " title="" />
                </div>
                <div class="item">
                    <img src="<?=base_url().WEBASSETS?>img/logos/img3.jpg" class="img-responsive " title="" />
                </div>
                <div class="item">
                    <img src="<?=base_url().WEBASSETS?>img/logos/img4.jpg" class="img-responsive " title="" />
                </div>
                <div class="item">
                    <img src="<?=base_url().WEBASSETS?>img/logos/img5.jpg" class="img-responsive " title="" />
                </div>
                <div class="item">
                    <img src="<?=base_url().WEBASSETS?>img/logos/img6.jpg" class="img-responsive " title="" />
                </div>
                <?php endif; ?>

            </div>
        </div>
    </div>
</section>
<!------------------------------------------------------------------------------------------------------->