
<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
    <div class="section-header container">
        <div class="container-fluid">
            <h3>فروع المجموعة</h3>
        </div>
    </div>
</section>





<section class="sec-benefits our_departs">
    

    <div id="Benefits-Section">
        <div class="container-fluid">
            <div class="row text-center spaceAfterBeforTitleLine">
                <?php if (isset($branches) && $branches!= null && !empty($branches)): ?>
                <?php foreach ($branches as $row): ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 LightBlocksPanel BlueTheme wow fadeInUp " data-wow-delay=".3s">
                    <div class="panel-heading"></div>
                    <div class="panel LightBlocks-Data popover-wrapper">
                        <div class="BenefitsImage  Benefits-1">
                            <a href="<?=base_url()."single-branch?id=".$row->id?>">
                                <img src="<?=base_url().IMAGEPATH.$row->icon ?>"></a>
                        </div>
                        <p class="LightBlocks-Description"><?=$row->name?></p>
                        <p><?= word_limiter( $row->details,40 )?></p>
                    </div>
                </div>
                    <?php endforeach;?>
                <?php else: ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 LightBlocksPanel BlueTheme wow fadeInUp " data-wow-delay=".3s">
                    <div class="panel-heading"></div>
                    <div class="panel LightBlocks-Data popover-wrapper">
                        <div class="BenefitsImage  Benefits-1">
                            <a href="#">
                                <img src="<?=base_url().WEBASSETS?>img/icons/Download-Advertising-PNG-File.png"></a>
                        </div>
                        <p class="LightBlocks-Description">الدعاية والاعلان والتسويق</p>
                        <p>بمفهوم عصري وعالمي من خلال الخبرات الواسعة وعبر نماذج فريدة نعمل على تقديم ما يتناسب من طموحات شركائنا ضمن رسالة واقعية واضحة تساعد على فهم الإعلان من قبل المتلقي من خلال منظور دراسات متخصصة في الحلول الاعلانية</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 LightBlocksPanel GreenTheme wow fadeInUp" data-wow-delay=".4s">
                    <div class="panel-heading"></div>
                    <div class="panel LightBlocks-Data popover-wrapper">
                        <div class="BenefitsImage  Benefits-2">
                            <a href="#">
                                <img src="<?=base_url().WEBASSETS?>img/icons/car.png"></a>
                        </div>
                        <p class="LightBlocks-Description">الفريق الأول لرياضة السيارات</p>
                        <p>لضرورة واهمية وجود فريق لرياضة السيارات سعودي يهدف الى تأهيل ورعاية الشباب السعودي الشغوف بهذه الرياضة أنشئ الفريق ليشمل جميع أنواع رياضة السيارات</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 LightBlocksPanel MaroonTheme wow fadeInUp" data-wow-delay=".5s">
                    <div class="panel-heading"></div>
                    <div class="panel LightBlocks-Data popover-wrapper">
                        <div class="BenefitsImage  Benefits-3">
                            <a href="#">
                                <img src="<?=base_url().WEBASSETS?>img/icons/services.png"></a>
                        </div>
                        <p class="LightBlocks-Description">صيانة وتعديل السيارات الرياضية</p>
                        <p>استمرارا لدعم رياضة السيارات أنشئ المركز ليحتضن هذه الرياضة وايضا ليقدم خدمات الصيانة وتعديل السيارات بشتّى أنواعها وليكون الدليل والمرجع للشباب السعودي الشغوف بهذه الرياضة </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 LightBlocksPanel BurpleTheme wow fadeInUp" data-wow-delay=".3s">
                    <div class="panel-heading"></div>
                    <div class="panel LightBlocks-Data popover-wrapper">
                        <div class="BenefitsImage  Benefits-4">
                            <a href="#">
                                <img src="<?=base_url().WEBASSETS?>img/icons/it-icon-png-7.jpg"></a>
                        </div>
                        <p class="LightBlocks-Description">تقنية المعلومات</p>
                        <p>نقدم حلول تقنية متكاملة في بناء وادارة الانظمة التقنية لاننا نعتقد أن التقنية يجب ان تطوّع لخدمة المنشآت ونعمل على تبسيط الأدوات التقنية لجعلها قابلة للاستخدام بيسر وسهولة لكي نمكّن المنشآت من التركيز على عملها بدلا من تضييع الوقت في التعقيدات التقنية</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 LightBlocksPanel RedTheme wow fadeInUp" data-wow-delay=".4s">
                    <div class="panel-heading"></div>
                    <div class="panel LightBlocks-Data popover-wrapper">
                        <div class="BenefitsImage  Benefits-5">
                            <a href="#">
                                <img src="<?=base_url().WEBASSETS?>img/icons/Conference-icon.png"></a>
                        </div>
                        <p class="LightBlocks-Description">تنظيم المعارض والمؤتمرات</p>
                        <p>نعمل من خلال منظومة متكاملة على انشاء وتنفيذ وتسويق المعارض والمؤتمرات والاشراف عليها أيضا اخذين بعين الاعتبار افضل الممارسات العلمية والعملية لذلك , نسعى لتمكين شركائنا بتدريبهم وتطويرهم من خلال خدماتنا المتميزة اخذين بعين الاعتبار افضل الممارسات المحلية والعالمية بدءا من التفكير والتصميم ومن ثم التنفيذ وتقييم الأثر والنتائج من اجل التحسين </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 LightBlocksPanel PinkTheme wow fadeInUp" data-wow-delay=".5s">
                    <div class="panel-heading"></div>
                    <div class="panel LightBlocks-Data popover-wrapper">
                        <div class="BenefitsImage  Benefits-6">
                            <a href="#">
                                <img src="<?=base_url().WEBASSETS?>img/icons/site-setting.png"></a>
                        </div>
                        <p class="LightBlocks-Description">الصيانة والتشغيل</p>
                        <p>نعمل من خلال منظومة متكاملة نوفر من خلالها لشركائنا كل ما يلزم في مجال الصيانة والتشغيل ورسم خطة عمل كاملة وتنفيذها كذلك بالإضافة الى خبرات مستشارينا الواسعة نقدم خدمات استشارية فيه هذا الجانب تساعد على تقييم الوضع الراهن وافضل الممارسات العلمية والعملية التي تنهض بالنشاطات والشركات المتعثرة</p>
                    </div>
                </div>
                <?php endif ?> 
            </div>
        </div>
    </div>
</section>

