
<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>إتصل بنا</h1>

                <div class="divider-15 d-none d-xl-block"></div>
            </div>
        </div>
    </div>
</section>





<!-- Section: events -->
<section class="contact-sec">
    <div class="container ptop-40 pbottom-50">
        <div class="section-content">
            <div class="col-xs-12 no-padding">

                <div class="col-md-7 col-xs-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h3 class="title line-bottom">بيانات <span class="text-theme-color-2 ">التواصل</span></h3>

                    <div class="col-xs-12">
                        <div class="connect-icon">
                            <img src="<?=base_url().WEBASSETS?>img/icons/maps-and-flags.png">
                            <div class="left-connect">

                                <h5>
                                    <?=(isset($this->setting->address))? $this->setting->address:'العليا – الرياض – المملكة العربية السعودية' ;?>
                                    </h5>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="connect-icon">
                            <img src="<?=base_url().WEBASSETS?>img/icons/mailbox.png">
                            <div class="left-connect">
                                <h5>ص.ب: <?=(isset($this->setting->post_num))? $this->setting->post_num:'534 2555' ;?></h5>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="connect-icon">
                            <img src="<?=base_url().WEBASSETS?>img/icons/mobile-phone.png">
                            <div class="left-connect">
                                <h5>هاتف: <?=(isset($this->setting->phones))? $this->setting->phones:'+ 966 534 352 2555' ;?></h5>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="connect-icon">
                            <img src="<?=base_url().WEBASSETS?>img/icons/mobile.png">
                            <div class="left-connect">
                                <h5>جوال: <?=(isset($this->setting->mobile))? $this->setting->mobile:'+ 966 534 352 2555' ;?></h5>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="connect-icon">
                            <img src="<?=base_url().WEBASSETS?>img/icons/fax.png">
                            <div class="left-connect">
                                <h5>فاكس: <?=(isset($this->setting->fax))? $this->setting->fax:'534 352 ' ;?></h5>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="connect-icon">
                            <img src="<?=base_url().WEBASSETS?>img/icons/email.png">
                            <div class="left-connect">
                                <h5>البريد الإلكتروني:  <?=(isset($this->setting->emails))? $this->setting->emails:'info@srgsa.com' ;?></h5>
                            </div>

                        </div>
                    </div>







                </div>
                <div class="col-md-5 col-xs-12  wow fadeInUp contact" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h3 class="head text-white">اترك رسالة لنا</h3>
                    <form id="" class="bg-light" name="" action="" >
                        <!--------------------------------------------------->
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="form_name">الإسم بالكامل <small>*</small></label>
                                    <input id="form_name" name="name" type="text" placeholder="ضع اسمك" required="" class="form-control send-contact">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="form_email">البريد الإلكترونى <small>*</small></label>
                                    <input id="form_email" name="email" class="form-control send-contact" type="email" placeholder="Ahmed@gmail.com">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="form_name">الموضوع <small>*</small></label>
                                    <input id="form_name" name="subject" type="text" placeholder="الموضوع" required="" class="form-control send-contact">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="form_message">الرسالة <small>*</small></label>
                            <textarea id="form_message" name="message" class="form-control send-contact" rows="5" placeholder="...."></textarea>
                        </div>
                        <div class="form-group">

                            <button type="button" class="btn btn-block btn-dark btn-theme-colored btn-sm mt-20 pt-10 pb-10 send-contact-btn" data-loading-text="Please wait...">أرسل الأن</button>
                        </div>
                        <!--------------------------------------------------->
                    </form>
                </div>
            </div>

        </div>
    </div>

</section>
<hr>
<section class="map-location ptop-40 pbottom-50">
    <div class="section-header container pbottom-30">
        <div class="container-fluid">
            <h3>موقعنا على الخريطة</h3>
        </div>
    </div>
    <div class="container">

        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7250.2599054464!2d46.69108553652373!3d24.688059104858716!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e2f033bb824b417%3A0xdc9745cc4a78b2d2!2z2KfZhNi52YTZitin2Iwg2KfZhNix2YrYp9i2INin2YTYs9i52YjYr9mK2Kk!5e0!3m2!1sar!2seg!4v1573587545501!5m2!1sar!2seg" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
</section>


