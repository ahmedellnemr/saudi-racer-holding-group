<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
    <div class="section-header container">
        <div class="container-fluid">
            <h3>
             مكتبة الفيديوهات
            </h3>
        </div>
    </div>
</section>

<section class="main_content pbottom-30 ptop-30">
    <div class="container">
  
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " id="secondDiv">
            <div class="background-white content_page"> 
                <div class="gallery_videos pbottom-20">
                
                    <div id="myList" class="list-unstyled">
                        <?php
                        if (isset($videos) && !empty($videos)){
                            foreach ($videos as  $video){
                                ?>

                                <div class="col-sm-10 col-sm-offset-1">
                                    <div class="box-video mbottom-50">
                                        <h5 class="text-center mbottom-20"><?= $video->video_title?></h5>
                                        <iframe width="100%" height="415" src="https://www.youtube.com/embed/<?= $video->video_link?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                        <?php

                            }
                        }
                        ?>

                    </div>

                  <!--  <div class="col-xs-12 text-center">
                        <button class="btn btn-load read-more" id="loadMore">مشاهدة أكثر</button>
                        

                    </div>-->

                </div>


            </div>
        </div>
    </div>
</section>


