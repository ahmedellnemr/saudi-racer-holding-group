

<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
    <div class="section-header container">
        <div class="container-fluid">
            <h3>
              صور الألبوم
          </h3>
      </div>
  </div>
</section>




<section class="main_content pbottom-30 ptop-30">
    <div class="container-fluid">

        <div class="col-lg-12 col-md-12 col-sm-10 col-xs-12 " id="secondDiv">
            <div class="background-white content_page">
                <div class="gallery">
                    <?php
                    if (isset($album) && !empty($album)) {
                        ?>

                        <ul class="clearfix list-unstyled gallery_imgs " id="thumbnails">
                            <?php

                            foreach ($album as $row) {
                                ?>

                                <li class="col-md-4 col-sm-6 padding-4">
                                    <div class="box-img">

                                        <a href="<?=base_url()."uploads/images/".$row->img?>"
                                         title="">
                                         <img src="<?=base_url()."uploads/images/".$row->img?>"
                                         alt="turntable" width="100%" height="250">
                                     </a>
                                 </div>
                             </li>

                             <?php
                         }
                         ?>
                     </ul>
                     <?php
                 }
                 ?>
             </div>



         </div>
     </div>
 </div>
</section>


