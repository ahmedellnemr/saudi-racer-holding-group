
<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>المركز الإعلامي</h1>
                <div class="divider-15 d-none d-xl-block"></div>
            </div>
        </div>
    </div>
</section>


<section class="all-blogs ptop-40 pbottom-40">
    <div class="container-fluid">
        <?php if (isset($news) && !empty($news)): ?>
            <?php foreach ($news as $row): ?>
                <div class="col-md-6 col-sm-6 col-xs-12  no-pxs">
                    <div class="post-slide">
                        <div class="post-img">
                            <a href="<?=base_url()."single-news?id=".$row->id?>">
                                <img src="<?= base_url() . IMAGEPATH .$row->logo?>" alt="">
                                <div class="post-date">
                                    <span class="date"><?=date("d",$row->date)?></span>
                                    <span class="month"><?=date("M",$row->date)?></span> 
                                </div>
                            </a>
                        </div>
                        <div class="post-review">
                            <h3 class="post-title">
                                <a href="<?=base_url()."single-news?id=".$row->id?>"><?=word_limiter($row->title,13)?></a>
                            </h3>
                            <ul class="post-bar">
                                <li><i class="fa fa-user"></i>
                                    <a href="<?=base_url()."single-news?id=".$row->id?>">admin</a>
                                </li>
                            </ul>
                            <p class="post-description"> 
                                <?=word_limiter($row->details,22)?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        <?php else: ?>
         <!--
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="post-slide">
                    <div class="post-img">
                        <a href="#">
                            <img src="<?= base_url() . WEBASSETS ?>img/news/2_50.jpg" alt="">
                            <div class="post-date">
                                <span class="date">13</span>
                                <span class="month">jan</span>
                            </div>
                        </a>
                    </div>
                    <div class="post-review">
                        <h3 class="post-title"><a href="#">بطولة الراليات</a></h3>
                        <ul class="post-bar">
                            <li><i class="fa fa-user"></i><a href="#">admin</a></li>
                        </ul>
                        <p class="post-description">حصل المتسابق السعودي الجديد في سباقات الراليات احمد المالكي ابن
                            البطل السعودي محمد المالكي على المركز الـ 7 في الترتيب العام .</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="post-slide">
                    <div class="post-img">
                        <a href="#">
                            <img src="<?= base_url() . WEBASSETS ?>img/news/2_50.jpg" alt="">
                            <div class="post-date">
                                <span class="date">13</span>
                                <span class="month">jan</span>
                            </div>
                        </a>
                    </div>
                    <div class="post-review">
                        <h3 class="post-title"><a href="#">بطولة الراليات</a></h3>
                        <ul class="post-bar">
                            <li><i class="fa fa-user"></i><a href="#">admin</a></li>
                        </ul>
                        <p class="post-description">حصل المتسابق السعودي الجديد في سباقات الراليات احمد المالكي ابن
                            البطل السعودي محمد المالكي على المركز الـ 7 في الترتيب العام .</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="post-slide">
                    <div class="post-img">
                        <a href="#">
                            <img src="<?= base_url() . WEBASSETS ?>img/news/2_50.jpg" alt="">
                            <div class="post-date">
                                <span class="date">13</span>
                                <span class="month">jan</span>
                            </div>
                        </a>
                    </div>
                    <div class="post-review">
                        <h3 class="post-title"><a href="#">بطولة الراليات</a></h3>
                        <ul class="post-bar">
                            <li><i class="fa fa-user"></i><a href="#">admin</a></li>
                        </ul>
                        <p class="post-description">حصل المتسابق السعودي الجديد في سباقات الراليات احمد المالكي ابن
                            البطل السعودي محمد المالكي على المركز الـ 7 في الترتيب العام .</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="post-slide">
                    <div class="post-img">
                        <a href="#">
                            <img src="<?= base_url() . WEBASSETS ?>img/news/2_50.jpg" alt="">
                            <div class="post-date">
                                <span class="date">13</span>
                                <span class="month">jan</span>
                            </div>
                        </a>
                    </div>
                    <div class="post-review">
                        <h3 class="post-title"><a href="#">بطولة الراليات</a></h3>
                        <ul class="post-bar">
                            <li><i class="fa fa-user"></i><a href="#">admin</a></li>
                        </ul>
                        <p class="post-description">حصل المتسابق السعودي الجديد في سباقات الراليات احمد المالكي ابن
                            البطل السعودي محمد المالكي على المركز الـ 7 في الترتيب العام .</p>
                    </div>
                </div>
            </div>
            -->
            <div class="alert alert-danger"><h5>لاتوجد اخبار </h5></div>
        <?php endif; ?>
        <div class="col-xs-12 text-center">
            <nav aria-label="Page navigation example">
                <?php if (isset($news) && !empty($news)): ?>
                    <?= $links ?>
                <?php else: ?>
                    <!--<ul class="pagination justify-content-center">
                        <li class="page-item "><a class="page-link pagina" href="#" tabindex="-1">السابق</a></li>
                        <li class="page-item"><a class="page-link pagina" href="#">1</a></li>
                        <li class="page-item"><a class="page-link pagina actief" href="#">2</a></li>
                        <li class="page-item"><a class="page-link pagina" href="#">3</a></li>
                        <li class="page-item "><a class="page-link pagina" href="#">التالي</a></li>
                    </ul>-->
                    <?= $links ?>
                <?php endif; ?>
            </nav>

        </div>

    </div>
</section>
	


