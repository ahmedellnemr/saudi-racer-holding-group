
<footer class="footer">
    <div class="container">

        <div class="footer-inner border-decor_top">
            <div class="col-xs-12 no-padding">
                <div class="col-sm-3">
                    <section id="text-2" class="footer-section widget_text">
                        <h3 class="footer-title">عن الشركة</h3>
                        <div class="textwidget">
                            <!--<a href="javascript:void(0);">
                                <img class="footer-logo img-responsive" src="<?=base_url().WEBASSETS?>img/logo-footer.png" alt="Logo">
                            </a>-->
                            <div class="footer-info">
                                <?php if (isset($this->setting->ar_about)): ?>
                                <?=word_limiter($this->setting->ar_about,35)?>
                                <?php else: ?>
                                    "تعتبر وكالة المتسابق السعودي للدعاية والإعلان والتسويق التي أسسها رجل الاعمال وبطل الشرق الأوسط للراليات / د. محمد بن احمد المالكي في منتصف الثمانينات الميلادية هي اللبنة الأولى لمجموعة المتسابق السعودي القابضة ...
                                <?php endif ?>

                                <a href="<?=base_url().'about-us'?>">المزيد</a> .
                            </div>
                            <div class="footer-contacts footer-contacts_mod-a">
                                <i class="icon fa fa-globe"></i>
                                <address class="footer-contacts-inner">
                                    <?php if (isset($this->setting->ar_address)): ?>
                                        <?=$this->setting->ar_address?>
                                    <?php else: ?>
                                        العليا – الرياض – المملكة العربية السعودية
                                    <?php endif ?>
                                </address>
                            </div>
                            <div class="footer-contacts">
                                <i class="icon fa fa-phone"></i>
                                <span class="footer-contacts-inner">اتصل على <b dir="ltr">
                                      <?php if (isset($this->setting->phones)): ?>
                                          <?=$this->setting->phones?>
                                      <?php else: ?>
                                          + 966 534 352 2555
                                      <?php endif ?>
                                    </b></span>
                            </div>
                            <div class="footer-contacts">
                                <i class="icon fa fa-envelope"></i>
                                <a class="footer-contacts-inner" href="">
                                    <?php if (isset($this->setting->emails)): ?>
                                    <?=$this->setting->emails?>
                                    <?php else: ?>
                                    info@srgsa.com
                                    <?php endif ?>
                                </a>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-sm-2">
                    <section id="nav_menu-2" class="footer-section widget_nav_menu">
                        <h3 class="footer-title">روابط هامة</h3>
                        <div class="menu-short-container">
                            <ul id="menu-short" class="menu footer-list ">
                                <li id="menu-item-1629" class="footer-list-item">
                                    <a href="<?=base_url()?>" class="footer-list-link">الرئيسية</a>
                                </li>
                                <li id="menu-item-2408" class="footer-list-item">
                                    <a href="<?=base_url().'about-us'?>" class="footer-list-link">عن المجموعة</a>
                                </li>
                                <li id="menu-item-2409" class=" footer-list-item">
                                    <a href="<?=base_url().'our-branches'?>" class="footer-list-link">فروعنا</a>
                                </li>
                                <li id="menu-item-1630" class="footer-list-item">
                                    <a href="<?=base_url().'our-news'?>" class="footer-list-link">المركز الإعلامي</a>
                                </li>
                                <li id="menu-item-2410" class="footer-list-item">
                                    <a href="<?=base_url().'contact-us'?>" class="footer-list-link">إتصل بنا</a>
                                </li>
                            </ul>
                        </div>
                    </section>
                </div>
                <div class="col-sm-3">
                    <section id="academica_twitter-2" class="footer-section widget_text">
                        <h3 class="footer-title">أخر الأخبار</h3>
                        <?php if (isset($this->news) && !empty($this->news)): ?>
                        <?php foreach ($this->news as $row): ?>
                            <div class="tweets">
                                <div class="tweets-text">
                                    <a href="<?=base_url()."single-news?id=".$row->id?>">
                                    <?=$row->title?>
                                    </a>
                                </div>
                                <?=word_limiter($row->details,18)?>
                                <span class="tweets-time"><?=calculate_from_time($row->date)?></span>
                            </div>
                            <?php endforeach;?>
                        <?php else: ?>
                        <div class="tweets">
                            <div class="tweets-text">
                                <a href="#">فوز المتسابق الدولى أحمد المالكي</a>
                            </div>
                            <p>"لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا .</p>

                            <span class="tweets-time">منذ 4 أيام</span>
                        </div>
                        <div class="tweets">
                            <div class="tweets-text">
                                <a href="#">فوز المتسابق الدولى أحمد المالكي</a>
                            </div>
                            <p>"لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا .</p>

                            <span class="tweets-time">منذ 4 أيام</span>
                        </div>
                        <?php endif;?>
                       <!-- <a class="tweets-link" href="https://twitter.com/">تابعنا على @تويتر</a>-->
                    </section>
                </div>
                <div class="col-sm-4">
                    <section id="academica_quick_contact-2" class="footer-section widget_text">
                        <h3 class="footer-title">موقعنا على الخريطة</h3>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3625.34835481697!2d46.696462285863944!3d24.680548984139147!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e2f0324fbfc7b9f%3A0x2468e9438dd753c!2z2YXYrNmF2YjYudipINin2YTZhdiq2LPYp9io2YIg2KfZhNiz2LnZiNiv2Yo!5e0!3m2!1sar!2ssa!4v1580708928742!5m2!1sar!2ssa" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        <!--<form class="form" method="POST">
                            <div class="form-group">
                                <input class="form-control send-contact" type="text" placeholder="اسمك .." name="name">
                                <input class="form-control send-contact" type="email" placeholder="البريد الإلكترونى" name="email">
                                <input class="form-control send-contact" type="text" placeholder="الموضوع" name="subject">
                                <textarea class="form-control send-contact" rows="7" placeholder="اترك رسالتك" name="message"></textarea>
                                
                            </div>
                            <div class="text-center">
                            <button type="button" class="btn btn-success btn-effect send-contact-btn" name="quick-contact">أرسل رسالتك</button>
                            </div>
                        </form>-->
                    </section>
                </div>
            </div>
        </div><!-- end footer-inner -->
        <div class="footer-bottom">
            <div class="copyright">

                <p>جميع الحقوق محفوظة © لدى شركة <a href="http://srgsa.com">أثير لتقنية المعلومات </a> .</p>
            </div>
            <ul class="social-links list-unstyled">
                <li><a class="icon fa fa-facebook" href="<?=(isset($this->setting->facebook))? $this->setting->facebook:"#"?>"></a></li>
               <!-- <li><a class="icon fa fa-linkedin" href=""></a></li>-->
                
                <li><a class="icon fa fa-twitter" href="<?=(isset($this->setting->twitter))? $this->setting->twitter:"#"?>"></a></li>
                <li><a class="icon fa fa-youtube" href="<?=(isset($this->setting->youtube))? $this->setting->youtube:"#"?>"></a></li>
                <li><a class="icon fa fa-instagram" href="<?=(isset($this->setting->instagram))? $this->setting->instagram:"#"?>"></a></li>
                
                <li><a class="icon fa fa-linkedin" href="<?=(isset($this->setting->linkedin))? $this->setting->linkedin:"#"?>"></a></li>
                <li><a class="icon fa fa-snapchat-ghost" href="<?=(isset($this->setting->snapchat_ghost))? $this->setting->snapchat_ghost:"#"?>"></a></li>
            </ul>
        </div><!-- end footer-bottom -->
    </div><!--/.container -->
</footer>



<script type="text/javascript" src="<?=base_url().WEBASSETS?>js/jquery-1.10.1.min.js"></script>
<script src="<?=base_url().WEBASSETS?>js/bootstrap-arabic.min.js"></script>
<script src="<?=base_url().WEBASSETS?>js/bootstrap-select.min.js"></script>
<script src="<?=base_url().WEBASSETS?>js/jquery.datetimepicker.full.js"></script>
<script src="<?=base_url().WEBASSETS?>js/jquery.easing.min.js"></script>
<script src="<?=base_url().WEBASSETS?>js/jquery.lightbox-0.5.min.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
<script src="<?=base_url().WEBASSETS?>js/owl.carousel.min.js?<?php echo date('l jS \of F Y h:i:s A')?>"></script>
<script src="<?=base_url().ASS?>sweetalert/sweetalert.js"></script>

<script src="<?=base_url().WEBASSETS?>js/custom.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
<script src="<?=base_url().WEBASSETS?>js/wow.min.js"></script>


<?php if (isset($my_footer)) { ?>

<?php if(in_array("revolution",$my_footer)):?>

<!-- Revolution Slider 5.x SCRIPTS -->
<script src="<?=base_url().WEBASSETS?>plugin/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="<?=base_url().WEBASSETS?>plugin/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
      The following part can be removed on Server for On Demand Loading) --> 
      <script type="text/javascript" src="<?=base_url().WEBASSETS?>plugin/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script> 
      <script type="text/javascript" src="<?=base_url().WEBASSETS?>plugin/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script> 
      <script type="text/javascript" src="<?=base_url().WEBASSETS?>plugin/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script> 
      <script type="text/javascript" src="<?=base_url().WEBASSETS?>plugin/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script> 
      <script type="text/javascript" src="<?=base_url().WEBASSETS?>plugin/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script> 
      <script type="text/javascript" src="<?=base_url().WEBASSETS?>plugin/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script> 
      <script type="text/javascript" src="<?=base_url().WEBASSETS?>plugin/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script> 
      <script type="text/javascript" src="<?=base_url().WEBASSETS?>plugin/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script> 
      <script type="text/javascript" src="<?=base_url().WEBASSETS?>plugin/revolution-slider/js/extensions/revolution.extension.video.min.js"></script>


 <!-- end .rev_slider_wrapper -->
      <script>
      	$(document).ready(function(e) {
      		$(".rev_slider").revolution({
      			sliderType:"standard",
      			sliderLayout: "auto",
      			dottedOverlay: "none",
      			delay: 5000,
      			navigation: {
      				keyboardNavigation: "off",
      				keyboard_direction: "horizontal",
      				mouseScrollNavigation: "off",
      				onHoverStop: "off",
      				touch: {
      					touchenabled: "on",
      					swipe_threshold: 75,
      					swipe_min_touches: 1,
      					swipe_direction: "horizontal",
      					drag_block_vertical: false
      				},
      				arrows: {
      					style:"zeus",
      					enable:true,
      					hide_onmobile:true,
      					hide_under:600,
      					hide_onleave:true,
      					hide_delay:200,
      					hide_delay_mobile:1200,
      					tmp:'<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div> </div>',
      					left: {
      						h_align:"left",
      						v_align:"center",
      						h_offset:30,
      						v_offset:0
      					},
      					right: {
      						h_align:"right",
      						v_align:"center",
      						h_offset:30,
      						v_offset:0
      					}
      				},
      				bullets: {
      					enable:true,
      					hide_onmobile:true,
      					hide_under:600,
      					style:"metis",
      					hide_onleave:true,
      					hide_delay:200,
      					hide_delay_mobile:1200,
      					direction:"horizontal",
      					h_align:"center",
      					v_align:"bottom",
      					h_offset:0,
      					v_offset:30,
      					space:5,
      					tmp:'<span class="tp-bullet-img-wrap">  <span class="tp-bullet-image"></span></span><span class="tp-bullet-title">{{title}}</span>'
      				}
      			},
      			responsiveLevels: [1240, 1024, 778],
      			visibilityLevels: [1240, 1024, 778],
      			gridwidth: [1170, 1024, 778, 480],
      			gridheight: [600, 768, 960, 350],
      			lazyType: "none",
      			parallax: {
      				origo: "slidercenter",
      				speed: 1000,
      				levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
      				type: "scroll"
      			},
      			shadow: 0,
      			spinner: "off",
      			stopLoop: "on",
      			stopAfterLoops: 0,
      			stopAtSlide: -1,
      			shuffle: "off",
      			autoHeight: "off",
      			fullScreenAutoWidth: "off",
      			fullScreenAlignForce: "off",
      			fullScreenOffsetContainer: "",
      			fullScreenOffset: "0",
      			hideThumbsOnMobile: "off",
      			hideSliderAtLimit: 0,
      			hideCaptionAtLimit: 0,
      			hideAllCaptionAtLilmit: 0,
      			debugMode: false,
      			fallbacks: {
      				simplifyAll: "off",
      				nextSlideOnWindowFocus: "off",
      				disableFocusListener: false,
      			}
      		});
      	});
      </script>
<!-- Slider Revolution Ends -->
<?php endif;  ?>
<?php } ?>



<script>
    $(".send-contact-btn").on('click', function() {
        //console.log("a")
        var obj = $(this) ;
        var $form = obj.closest('form');
        //----------------------------------------
        var cbs = document.getElementsByClassName('send-contact');
        var flagValid = true;
        /*
        for (var i = 0; i < cbs.length; i++) {
            var validClass =$(cbs[i]).hasClass("valid-error") ;
            if(cbs[i].value == ""  ){
                if( validClass != true){
                    $(cbs[i]).addClass("valid-error");
                    $(cbs[i]).after('<span style="color: red" class="valid-span"> هذا الحقل ضرورى  </span>');
                }
                flagValid = false;
            }else{
                $(cbs[i]).next(".valid-span").remove();
                $(cbs[i]).removeClass("valid-error");
            }
        }
        */
        //----------------------------------------
        //console.log("b" + flagValid )
        //----------------------------------------
        if(flagValid == true){
            var dataString = $(".send-contact",$form).serialize();
            console.log(dataString)
            var msg = '<?=($this->webLang == "ar")? "شكرا لتواصلك معنا":"Thank for contact with us";?>';
            var msgTitle = '<?=($this->webLang == "ar")? "تم الإرسال !":"Send success nessage !";?>';
            $.ajax({
                type:'post',
                url: '<?php echo base_url().$this->webLang ?>/web/contact',
                data:dataString,
                dataType: 'html',
                cache:false,
                success: function(html){
                    //console.log(html )
                    swal({
                            title:msgTitle,
                            text: msg,
                            type: "success",
                        },
                        function(){
                            $(".send-contact").val("");
                        }
                    );

                    /*
                    $("#contact-result").append('<div class="alert alert-info  alert-rounded">\n' +
                        '                       <i class="ti-user"></i>\n'  + html +
                        '                       <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                        '                       <span aria-hidden="true">×</span> </button>\n' +
                        '                      </div>');
                    */
                },
                error:function(error){
                    console.log(error.responseText);
                }
            });
        }
    });
</script>




<script>
    new WOW().init();
    $('.datepicker').datetimepicker({
        format:'Y-m-d',
        time: false
    });
</script>

<script type="text/javascript">
    $(function() {
        $('#thumbnails a').lightBox();
    });

    $(document).scroll(function() {
        var y = $(this).scrollTop();
        if (y > 100) {
            $('.fixed-social').fadeIn();
        } else {
            $('.fixed-social').fadeOut();
        }
    });
</script>

</body>
</html>
