<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
      <meta name="description" content="تعتبر وكالة المتسابق السعودي للدعاية والإعلان والتسويق التي أسسها رجل الاعمال وبطل الشرق الأوسط للراليات / د. محمد بن احمد المالكي في منتصف الثمانينات الميلادية هي اللبنة الأولى لمجموعة المتسابق السعودي القابضة ">
    <meta name="author" content="مجموعة المتسابق السعودي ">
    <link rel="canonical" href="https://srgsa.com/" />

    <title>مجموعة المتسابق السعودي </title>
    <link rel="icon" type="image/png" sizes="32x32" href="<?=base_url().WEBASSETS?>img/favicon.png">


    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/bootstrap-arabic-theme.min.css" />
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/bootstrap-arabic.min.css" />
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/bootstrap-select.min.css" >
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/jquery.datetimepicker.css" >
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/owl.carousel.css?<?php echo date('l jS \of F Y h:i:s A')?>" >
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/owl.theme.css?<?php echo date('l jS \of F Y h:i:s A')?>" >
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/jquery.lightbox-0.5.css" >
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/animate.css">
    <link rel="stylesheet" href="<?=base_url().ASS?>sweetalert/sweetalert.css">
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/style.css?<?php echo date('l jS \of F Y h:i:s A')?>">
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/responsive.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">

<?php if (isset($my_footer)) { ?>

<?php if(in_array("revolution",$my_footer)):?>
	<!-- Revolution Slider 5.x CSS settings -->
	<link  href="<?=base_url().WEBASSETS?>plugin/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
	<link  href="<?=base_url().WEBASSETS?>plugin/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
	<link  href="<?=base_url().WEBASSETS?>plugin/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
<?php endif;  ?>
<?php } ?>


<?php $actual_link = current_url();?>
<link rel="canonical" href="<?php echo $actual_link?>">
<meta property="og:locale" content="en_US">
<meta property="og:type" content="article">
<meta property="og:title" content='<?php  if(isset($share_title)){echo trim(strip_tags($share_title), '"');}else{ echo "مجموعة المتسابق السعودي";} ?>' >
<meta property="og:description" content='<?php  if(isset($share_content)){echo trim(strip_tags($share_content), '"');}else{ echo "تعتبر وكالة المتسابق السعودي للدعاية والإعلان والتسويق التي أسسها رجل الاعمال وبطل الشرق الأوسط للراليات / د. محمد بن احمد المالكي في منتصف الثمانينات الميلادية هي اللبنة الأولى لمجموعة المتسابق السعودي القابضة";} ?>'>
<meta property="og:url" content="<?php echo $actual_link?>">
<meta property="og:site_name" content="Yoko Co.">
<meta property="article:tag" content="facebook">
<meta property="article:tag" content="wordpress">
<meta property="article:section" content="Advice">
<meta property="article:published_time" content="<?=date("Y-m-d")?>T11:22:16-04:00">
<meta property="article:modified_time" content="<?=date("Y-m-d")?>T16:31:30-04:00">
<meta property="og:updated_time" content="<?=date("Y-m-d")?>4T16:31:30-04:00">
<meta property="og:image" content="<?php if(isset($share_image)){ echo  base_url().$share_image ;} else{ echo base_url()."assets/frontend/img/logo.png"; }?>">
<meta property="og:image:secure_url" content="<?php if(isset($share_image)){ echo  base_url().$share_image ;} else{ echo base_url()."assets/frontend/img/logo.png" ;} ?>">
<meta property="og:image:width" content="3428">
<meta property="og:image:height" content="2332">




</head>

<body id="page-top" data-spy="scroll" >


<header class="mheader">
    <div class="top-nav">
        <div class="container-fluid">
            <div class="col-md-5 col-sm-6 col-xs-12 padding-4">
                <div class="head-contact">
                    <a href="#" >
                        
                            <img src="<?= base_url() . WEBASSETS ?>img/icons/mobile-phone.png">
                        

                        <?php if (isset($this->setting->phones)): ?>
                            <span dir="ltr"><?=$this->setting->phones?></span>
                        <?php else: ?>
                            <span dir="ltr">+ 966 114661277</span>
                        <?php endif ?>
                    </a>
                    
                    
                    <a href="#" >
                        
                        
                            <img src="<?= base_url() . WEBASSETS ?>img/wa-1.png">
                        

                        <?php if (isset($this->setting->mobile)): ?>
                            <span dir="ltr"><?=$this->setting->mobile?></span>
                        <?php else: ?>
                            <span dir="ltr">+ 966505226181</span>
                        <?php endif ?>
                    </a>
                    
                    
                    <a href="#">
                        <img src="<?=base_url().WEBASSETS?>img/email-logo-png.png">
                        <?php if (isset($this->setting->emails)): ?>
                            <?=$this->setting->emails?>
                        <?php else: ?>
                            srg@srgsa.com
                        <?php endif ?>

                    </a>
                </div>


            </div>
            <div class="col-md-3 hidden-xs hidden-sm padding-4">
                 <a href=""  class="typewrite" data-period="2000" data-type='[ "الموقع تحت التطوير ( نسخة تجريبية ) ", "تدشين الموقع الإلكتروني للمتسابق السعودي" ]'>
						<span class="wrap"></span>
				</a>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="text-center social-links">
                    <!-- Add font awesome icons -->
                    <a href="<?=(isset($this->setting->facebook))? $this->setting->facebook:"#"?>" target="_blank" class="facebook" title="Facebook">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="<?=(isset($this->setting->twitter))? $this->setting->twitter:"#"?>" class="twitter" target="_blank" title="Twitter">
                        <i class="fa fa-twitter"></i></a>
                    <a href="<?=(isset($this->setting->youtube))? $this->setting->youtube:"#"?>" class="youtube" target="_blank" title="Youtube">
                        <i class="fa fa-youtube"></i></a>
                    <a href="<?=(isset($this->setting->instagram))? $this->setting->instagram:"#"?>" class="instagram" target="_blank" title="Instagram">
                        <i class="fa fa-instagram"></i></a>
                        
                    <a href="<?=(isset($this->setting->linkedin))? $this->setting->linkedin:"#"?>" class="linkedin" target="_blank" title="linkedin">
                    <i class="fa fa-linkedin"></i></a>
                    
                    <a href="<?=(isset($this->setting->snapchat_ghost))? $this->setting->snapchat_ghost:"#"?>" class="snapchat_ghost" target="_blank" title="snapchat_ghost">
                    <i class="fa fa-snapchat-ghost "></i></a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
		<div class="borders ">
			<div class="border box box-1">
			</div>
			<div class="border box box-1">
			</div> 
		</div>
	</div>
        
    <nav class="navbar navbar-default" data-spy="affix" data-offset-top="150">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=base_url()?>">
                    <?php if (isset($this->setting->logo)): ?>
                        <img src="<?= base_url() . IMAGEPATH.$this->setting->logo ?>">
                    <?php else: ?>
                        <img src="<?= base_url() . WEBASSETS ?>img/wa-1.png">
                    <?php endif ?>
                </a>
            </div>
 
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav main-links">
                    <li class="active"><a href="<?=base_url()?>">الرئيسية <span class="sr-only">(current)</span></a></li>
                    <li><a href="<?=base_url().'about-us'?>">عن المجموعة</a></li>
                    <li><a href="<?=base_url().'our-branches'?>">فروعنا</a></li>
                    <li><a href="<?=base_url().'our-news'?>">المركز الإعلامى</a></li>
                    <li><a href="<?=base_url().'Web/gallery'?>">مكتبة الصور</a></li>
                    <li><a href="<?=base_url().'Web/videos'?>">مكتبة الفيديوهات</a></li>
                   
                    <li><a href="<?=base_url().'contact-us'?>">إتصل بنا</a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="saudi-vision">
                        <a href="<?=base_url()?>">
                            <img src="<?=base_url().WEBASSETS?>img/Saudi_Vision_2030_logo.png">
                        </a>
                    </li>

                </ul>


            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>



<!--
<div class="fixed-social ">
    <ul>
        <li class="facebook">
            <i class="fa fa-facebook" aria-hidden="true"></i>
            <div class="slider-text">
                <p>facebook</p>
            </div>
        </li>
        <li class="twitter">
            <i class="fa fa-twitter" aria-hidden="true"></i>
            <div class="slider-text">
                <p>twitter</p>
            </div>
        </li>
        <li class="instagram">
            <i class="fa fa-instagram" aria-hidden="true"></i>
            <div class="slider-text">
                <p>instagram</p>
            </div>
        </li>
        <li class="google">
            <i class="fa fa-google" aria-hidden="true"></i>
            <div class="slider-text">
                <p>google</p>
            </div>
        </li>
        <li class="whatsapp">
            <i class="fa fa-whatsapp" aria-hidden="true"></i>
            <div class="slider-text">
                <p>whatsapp</p>
            </div>
        </li>
    </ul>
</div>
-->



