
<style type="text/css">
    .gallery .panel-default>.panel-heading {
    color: #333;
    background-color: #2b2b2b;
    border-color: #ddd;
    background-image: none;
   
}
.gallery .panel-title {
    margin-top: 0;
    margin-bottom: 0;
    font-size: 16px;
    color: #fff;
    font-weight: bold;
    text-align: center;
    padding: 6px 0px;
}
.gallery .panel img {
    height: 300px;
    width: 100%;
}
  .gallery .panel-body {
    overflow: hidden;
  }
  .gallery .panel-body img:hover {
        transform: scale(1.05,1.05);
        -webkit-transition: all 0.3s ease-in-out 0s;
        -moz-transition: all 0.3s ease-in-out 0s;
        -ms-transition: all 0.3s ease-in-out 0s;
        -o-transition: all 0.3s ease-in-out 0s;
        transition: all 0.3s ease-in-out 0s;
    }
.gallery .panel-footer{
    text-align: center;
    background-color: #849f38;
    color: #fff;
}
</style>



<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
    <div class="section-header container">
        <div class="container-fluid">
            <h3>
              مكتبة الصور
            </h3>
        </div>
    </div>
</section>


<section class="main_content pbottom-30 ptop-30">
    <div class="container">
        <div class="gallery">
            <?php
            if (isset($library) && !empty($library)) {
                foreach ($library as $row){


                ?>
                <div class="col-md-4 col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading" >
                            <h3 class="panel-title"> <?= $row->title?></h3>
                        </div>
                        <div class="panel-body" style="padding: 0;">
                            <a target="_blank" href="<?= base_url()."Web/album/".$row->id?>">
                    <?php
                    if (isset($row->img) && !empty($row->img)) {
                        ?>
                        <img src="<?= base_url() . "uploads/images/" . $row->img ?>">
                        <?php
                    } else{
                        ?>
                        <img src="<?= base_url()."asisst/web_asset/img/no_image.jpg"?>"
                        <?php
                    }
                        ?>
                        </a>
                        </div>
                        <div class="panel-footer">عدد الصور (<?= $row->count?>)</div>

                    </div>
                </div>

                <?php
            }    }
            ?>


        </div>
    </div>
</section>
