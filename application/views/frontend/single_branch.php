
<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
    <div class="section-header container">
        <div class="container-fluid">
            <h3><?=$one->name?></h3>
        </div>
    </div>
</section>

<section class="single-depart ptop-40 pbottom-40">
    <div class="container-fluid pbottom-20">
        <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7">
            <h3 class="title line-bottom"><span class="text-theme-color-2 "><?=$one->name?></span></h3>
            <p class="paragraph"><?=$one->details?></p>

            <ul class="feats">

                <?php  $tags = get_arr($one->tags);
                if (is_array($tags) && !empty($tags)): ?>
                    <?php foreach ($tags as $key=>$vakue): ?>
                        <li class="col-sm-6 col-xs-6">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            <?=$vakue?>
                        </li>
                    <?php endforeach; ?>
                <?php endif ?>

            </ul>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9">
            <img src="<?=base_url().IMAGEPATH.$one->logo?>">
        </div>


    </div>

    <hr>

    <div class="container ptop-20">
        <div class="col-xs-12">
            <ul class=" list-unstyled departs-imgs " id="thumbnails">

                <?php if (isset($one->subs) && !empty($one->subs)): ?>
                    <?php foreach ($one->subs as $row): ?>
                        <li class="col-lg-3 col-md-4 col-sm-6 col-xs-12  wow fadeInUp no-padding">
                            <div class="box-img">
                                <a href="<?=base_url().IMAGEPATH.$row->name?>" title="مجموعة المتسابق السعودي">
                                    <img src="<?=base_url().IMAGEPATH.$row->name?>" alt="مجموعة المتسابق السعودي" width="100%">
                                </a>
                            </div>
                        </li>
                    <?php endforeach ?>
                <?php endif ?>



            </ul>
        </div>
    </div>
</section>



