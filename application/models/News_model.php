<?php
/**
 * Created by PhpStorm.
 * User: win 7
 * Date: 11/11/2019
 * Time: 9:09 PM
 */
class News_model extends MY_Model {
    public $_table = 'news';
    public $primary_key = 'id';
    /*
    protected $soft_delete = TRUE;
    protected $soft_delete_key = 'available';
    */

    public $belongs_to = [
         'user' => array( 'model' => 'system_management/User_model',"primary_key"=>'publisher' )
         ];
    public $has_many = ['subs' => array( 'model' => 'News_images_model','primary_key' => 'main_id' )];

    public $before_create = array( 'timestamps_in' );
    public $before_update = array( 'timestamps_up' );

    protected function timestamps_in($row)
    {
        $row['created_at'] = $row['updated_at'] =  date('Y-m-d H:i:s');
        return $row;
    }
    protected function timestamps_up($row)
    {
        $row['updated_at'] =  date('Y-m-d H:i:s');
        return $row;
    }

} // END CLASS