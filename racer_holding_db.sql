-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 14, 2019 at 08:53 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `racer_holding_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `branchs`
--

DROP TABLE IF EXISTS `branchs`;
CREATE TABLE IF NOT EXISTS `branchs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tags` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `branchs`
--

INSERT INTO `branchs` (`id`, `name`, `tags`, `logo`, `icon`, `details`, `created_at`, `updated_at`) VALUES
(1, 'الدعاية والاعلان والتسويق', ' فلاير , بروشور  ,  مجلة  ,  شنط ورقية ', 'b439b0cfff0dbf45d94e9a20c196c318.png', 'b439b0cfff0dbf45d94e9a20c196c318.png', 'بمفهوم عصري وعالمي من خلال الخبرات الواسعة وعبر نماذج فريدة نعمل على تقديم ما يتناسب من طموحات شركائنا ضمن رسالة واقعية واضحة تساعد على فهم الإعلان من قبل المتلقي من خلال منظور دراسات متخصصة في الحلول الاعلانية', '2019-11-12 21:30:12', '2019-11-12 21:30:12'),
(2, 'الفريق الأول لرياضة السيارات ', ' نتيجة سنوية ', 'fbabe2d0098b9496cf955854fcc9f8eb.png', 'fbabe2d0098b9496cf955854fcc9f8eb.png', 'لضرورة واهمية وجود فريق لرياضة السيارات سعودي يهدف الى تأهيل ورعاية الشباب السعودي الشغوف بهذه الرياضة أنشئ الفريق ليشمل جميع أنواع رياضة السيارات\r\n', '2019-11-12 21:32:34', '2019-11-12 21:32:34'),
(3, 'صيانة وتعديل السيارات الرياضية', 'صيانة وتعديل السيارات الرياضية', 'a3b46a13c90ca3b2248b7064f2b98582.png', 'a3b46a13c90ca3b2248b7064f2b98582.png', 'استمرارا لدعم رياضة السيارات أنشئ المركز ليحتضن هذه الرياضة وايضا ليقدم خدمات الصيانة وتعديل السيارات بشتّى أنواعها وليكون الدليل والمرجع للشباب السعودي الشغوف بهذه الرياضة \r\n', '2019-11-12 21:33:39', '2019-11-12 21:33:39'),
(4, 'تقنية المعلومات', 'بناء الأنظمة التقنية , برامج التوظيف والتوطين', '5534874e02a2e4e0185a442368f8edcc.jpg', '5534874e02a2e4e0185a442368f8edcc.jpg', 'نقدم حلول تقنية متكاملة في بناء وادارة الانظمة التقنية لاننا نعتقد أن التقنية يجب ان تطوّع لخدمة المنشآت ونعمل على تبسيط الأدوات التقنية لجعلها قابلة للاستخدام بيسر وسهولة لكي نمكّن المنشآت من التركيز على عملها بدلا من تضييع الوقت في التعقيدات التقنية\r\n\r\nنقدم حلول تقنية متكاملة في مجال الموارد البشرية حيث يتم توفير الكوادر البشرية المناسبة والمطابقة لمتطلبات وشروط المنشآت وذلك من خلال برنامج العمل عن بعد وهو احد برامجنا المعتمدة من وزارة العمل والتنمية الاجتماعية\r\n', '2019-11-12 21:34:47', '2019-11-12 21:34:47'),
(5, 'تنظيم المعارض والمؤتمرات', 'تنظيم المعارض والمؤتمرات , التدريب وتنمية القدرات , الابتكار الاجتماعي ', 'c1ea689a0d082fb6bc2e15a8d13e9d16.png', 'c1ea689a0d082fb6bc2e15a8d13e9d16.png', 'نعمل من خلال منظومة متكاملة على انشاء وتنفيذ وتسويق المعارض والمؤتمرات والاشراف عليها أيضا اخذين بعين الاعتبار افضل الممارسات العلمية والعملية لذلك\r\nنسعى لتمكين شركائنا بتدريبهم وتطويرهم من خلال خدماتنا المتميزة اخذين بعين الاعتبار افضل الممارسات المحلية والعالمية بدءا من التفكير والتصميم ومن ثم التنفيذ وتقييم الأثر والنتائج من اجل التحسين \r\nنعمل على تأسيس وتشغيل إدارات المسؤولية المجتمعية في المنشآت كما و نعمل على ابتكار وتصميم وتنفيذ البرامج النوعية الخاصة بالمسؤولية المجتمعية التي من شأنها تمكّن الجهات المستفيدة من تحقيق أهدافها وتعظم دورها الاجتماعي وذلك في اطار احترافي وفق افضل الممارسات التي تحقق النجاح وتضمن الاستدامة \r\n', '2019-11-12 21:35:56', '2019-11-12 21:35:56'),
(6, 'الصيانة والتشغيل', 'الصيانة والتشغيل', 'e74fdc4fded5a460a9a2f2510ba61b84.png', 'e74fdc4fded5a460a9a2f2510ba61b84.png', 'نعمل من خلال منظومة متكاملة نوفر من خلالها لشركائنا كل ما يلزم في مجال الصيانة والتشغيل ورسم خطة عمل كاملة وتنفيذها كذلك بالإضافة الى خبرات مستشارينا الواسعة نقدم خدمات استشارية فيه هذا الجانب تساعد على تقييم الوضع الراهن وافضل الممارسات العلمية والعملية التي تنهض بالنشاطات والشركات المتعثرة\r\n', '2019-11-12 21:36:51', '2019-11-12 21:36:51');

-- --------------------------------------------------------

--
-- Table structure for table `branchs_images`
--

DROP TABLE IF EXISTS `branchs_images`;
CREATE TABLE IF NOT EXISTS `branchs_images` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `main_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `branchs_images_main_id_foreign` (`main_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_replay` tinyint(4) NOT NULL DEFAULT '0',
  `replay` text COLLATE utf8mb4_unicode_ci,
  `date` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `forms_setting`
--

DROP TABLE IF EXISTS `forms_setting`;
CREATE TABLE IF NOT EXISTS `forms_setting` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `world_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fild_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `is_valid` tinyint(4) NOT NULL DEFAULT '1',
  `available` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `forms_setting`
--

INSERT INTO `forms_setting` (`id`, `type`, `world_key`, `page_link`, `fild_key`, `value`, `is_valid`, `available`) VALUES
(1, 'text', 'website', 'main_data', 'website', 'www.srgsa.com', 1, 1),
(2, 'text', 'address', 'main_data', 'ar_address', 'العليا – الرياض – المملكة العربية السعودية', 1, 1),
(3, 'text', 'en_address', 'main_data', 'en_address', NULL, 1, 0),
(4, 'tag', 'phones', 'main_data', 'phones', '+ 966 534 352 2555', 1, 1),
(5, 'tag', 'emails', 'main_data', 'emails', 'info@srgsa.com', 1, 1),
(6, 'area', 'about_us', 'main_data', 'ar_about', 'تعتبر وكالة المتسابق السعودي للدعاية والإعلان والتسويق التي أسسها رجل الاعمال وبطل الشرق الأوسط للراليات / د. محمد بن احمد المالكي في منتصف الثمانينات الميلادية هي اللبنة الأولى لمجموعة المتسابق السعودي القابضة ومن ثم عملنا على إنشاء مؤسسة المتسابق السعودي للإنتاج الفني تلاها تأسيس الفريق السعودي الأول لرياضة السيارات برعاية الرئيس العام لرعاية الشباب صاحب السمو الملكي الأمير / فيصل بن فهد بن عبدالعزيز (رحمه الله) وامتدادا لدعم وتطوير رياضة السيارات انشأنا مركز المتسابق السعودي لصيانة السيارات وتعديلها كما وعملنا على إنشاء أثير الوسطى لتقنية المعلومات و أثير الوسطى لتنظيم المعارض والمؤتمرات والدورات ونعمل في مجموعة المتسابق السعودي القابضة على ابتكار وتنفيذ كل ما يتعلق بهذه المجالات وفق معايير عالمية تواكب رؤية 2030 حيث نعمل وفق نماذج خاصة وعالمية لتحقيق الأهداف الاستراتيجية لشركائنا', 1, 1),
(7, 'area', 'en_about', 'main_data', 'en_about', NULL, 1, 0),
(8, 'img', 'logo', 'main_data', 'logo', 'ebc957d09fafa84f42a4afbf26091a0c.png', 1, 1),
(9, 'social', '', 'main_data', 'facebook', '#', 1, 1),
(10, 'social', '', 'main_data', 'twitter', '#', 1, 1),
(11, 'social', '', 'main_data', 'instagram', '#', 1, 1),
(12, 'social', '', 'main_data', 'linkedin', NULL, 1, 0),
(13, 'social', '', 'main_data', 'telegram', NULL, 1, 0),
(14, 'social', '', 'main_data', 'youtube', '#', 1, 1),
(15, 'social', '', 'main_data', 'google_plus', '#', 1, 1),
(16, 'social', '', 'main_data', 'snapchat_ghost', NULL, 1, 0),
(17, 'social', '', 'main_data', 'whatsapp', '#', 1, 1),
(18, 'social', '', 'main_data', 'dribbble', NULL, 1, 0),
(19, 'social', '', 'main_data', 'pinterest', NULL, 1, 0),
(20, 'tag', 'mobile', 'main_data', 'mobile', '+ 966 534 352 2555', 1, 1),
(21, 'text', 'post_num', 'main_data', 'post_num', '555 55 ', 1, 1),
(22, 'text', 'fax', 'main_data', 'fax', '555 55 ', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `vedio_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` int(11) NOT NULL,
  `publisher` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `news_publisher_foreign` (`publisher`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `logo`, `details`, `vedio_link`, `date`, `publisher`, `created_at`, `updated_at`) VALUES
(1, 'فوز المتسابق الدولى أحمد المالكي1', '21c26981a00d4f7261c75f2e71866234.jpg', 'حصل المتسابق السعودي الجديد في سباقات الراليات احمد المالكي ابن البطل السعودي محمد المالكي على المركز الـ 7 في الترتيب العام .', NULL, 1573592400, 1, '2019-11-12 22:25:23', '2019-11-12 22:25:23'),
(2, 'فوز المتسابق الدولى أحمد المالكي2', '21c26981a00d4f7261c75f2e71866234.jpg', 'حصل المتسابق السعودي الجديد في سباقات الراليات احمد المالكي ابن البطل السعودي محمد المالكي على المركز الـ 7 في الترتيب العام .', '5Gg0UmLBF10', 1573592400, 1, '2019-11-12 22:25:23', '2019-11-12 22:25:23'),
(3, 'فوز المتسابق الدولى أحمد المالكي3', '21c26981a00d4f7261c75f2e71866234.jpg', 'حصل المتسابق السعودي الجديد في سباقات الراليات احمد المالكي ابن البطل السعودي محمد المالكي على المركز الـ 7 في الترتيب العام .', NULL, 1573592400, 1, '2019-11-12 22:25:23', '2019-11-12 22:25:23');

-- --------------------------------------------------------

--
-- Table structure for table `news_images`
--

DROP TABLE IF EXISTS `news_images`;
CREATE TABLE IF NOT EXISTS `news_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `main_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `news_images_main_id_foreign` (`main_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_images`
--

INSERT INTO `news_images` (`id`, `main_id`, `name`) VALUES
(1, 1, 'swr-rshyfy-mn-rly-hyl.jpg'),
(2, 1, 'race.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ar_page_title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_page_title` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_link` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#',
  `perant_id` int(11) NOT NULL,
  `level` tinyint(4) DEFAULT '0',
  `page_order` tinyint(4) DEFAULT '0',
  `page_icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `available` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
CREATE TABLE IF NOT EXISTS `partners` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `name`, `logo`, `details`, `created_at`, `updated_at`) VALUES
(1, NULL, '60d8aff5653369f0286713a85995c895.png', NULL, '2019-11-12 21:55:34', '2019-11-12 21:55:34'),
(2, NULL, '6bbfd03e6cf38a3e27209be2306d8224.jpg', NULL, '2019-11-12 21:55:41', '2019-11-12 21:55:41'),
(3, NULL, '4cf599dbf8befc92ae7579103d6b71c4.jpg', NULL, '2019-11-12 21:55:47', '2019-11-12 21:55:47'),
(4, NULL, '359162d06403d05d74c301ffd95b5ee7.jpg', NULL, '2019-11-12 21:55:53', '2019-11-12 21:55:53'),
(5, NULL, 'fc8236d1cc3b1192fef82fed6f817ece.jpg', NULL, '2019-11-12 21:56:00', '2019-11-12 21:56:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `page_id` bigint(20) UNSIGNED NOT NULL,
  `add_btn` tinyint(4) DEFAULT '1',
  `edit_btn` tinyint(4) DEFAULT '1',
  `delete_btn` tinyint(4) DEFAULT '1',
  `approve_btn` tinyint(4) DEFAULT '1',
  `details_btn` tinyint(4) DEFAULT '1',
  `print_btn` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `permissions_user_id_foreign` (`user_id`),
  KEY `permissions_page_id_foreign` (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project_trans`
--

DROP TABLE IF EXISTS `project_trans`;
CREATE TABLE IF NOT EXISTS `project_trans` (
  `world_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  UNIQUE KEY `project_trans_world_key_unique` (`world_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_trans`
--

INSERT INTO `project_trans` (`world_key`, `ar_title`, `en_title`) VALUES
('about_us', 'من نحن', 'About Us'),
('address', 'العنوان', 'Address'),
('ar_about', 'نبذه عن الشركة بالعربية', 'Company Profile in Arabic'),
('ar_about_us', 'من نحن بالعربية', 'About Us in Arabic'),
('ar_address', 'العنوان بالعربية', 'Address in Arabic'),
('ar_Terms', ' الشروط و الاحكام بالعربية ', 'Terms and Conditions in Arabic'),
('arabic', 'العربية', 'Arabic'),
('availability', 'التوافر', 'Availability'),
('basic_information', 'البيانات الاساسية ', 'Basic Information'),
('contact_us', 'اتصل بنا', 'Contact Us'),
('copy_right', 'جميع الحقوق محفوظة لدى شركة ماوان الزراعية', 'Copy right'),
('correct_message', 'تاكد من إدخال البيانات الصحيحة ', 'correct User name or Password'),
('days', 'أيام', 'Days'),
('download', 'تحميل', 'Download'),
('emails', 'البريد الإلكتروني', 'Email'),
('en_about', 'نبذه عن الشركة بالانجليزية', 'Company Profile in English'),
('en_about_us', 'العنوان بالانجليزية', 'About Us in English'),
('en_address', 'العنوان بالانجليزية', 'Address in English'),
('en_Terms', 'الشروط و الاحكام بالانجليزية', 'Terms and Conditions in English'),
('english', 'الانجليزية', 'English'),
('fax', 'فاكس', 'Fax'),
('follow_all_new', ' تابع كل ماهو جديد', 'Follow all new'),
('Forget_Password', 'نسيت كلمة المرور', 'Forget Password ?'),
('home', 'الرئيسية', 'Home'),
('hours', 'ساعات', 'Hours'),
('Inbox', 'صندوق الوارد', 'Inbox'),
('last_login', 'اخر دخول', 'Last Login'),
('last_news', 'اخر الاخباار', 'last news'),
('location', 'موقع الشركة', 'Location'),
('login', 'تسجيل الدخول', 'Login'),
('logo', 'الشعار', 'Logo'),
('logout', 'تسجيل الخروج', 'Logout'),
('main_menu', 'القائمة الرئيسية', 'Main menu'),
('message', 'الرسالة', 'Message'),
('minuts', 'دقائق', 'Minuts'),
('mobile', 'جوال', 'mobile'),
('more', 'المزيد', 'More'),
('my_profile', 'ملفي', 'My Profile'),
('name', 'الإسم ', 'Name'),
('no_data', 'لا  يوجد بيانات', 'No data found'),
('our_clients', 'عملائنا', 'Our clients'),
('our_news', 'مقالات و أخبار شركة ماوان الزراعية', 'Our news'),
('our_vision', 'رؤيتنا', 'Our vision'),
('password', 'كلمة المرور', 'Password'),
('phone', 'الهاتف', 'Phone'),
('phone_number', 'هاتف رقم', 'Phone number'),
('phones', 'أرقام الاتصال', 'Phones'),
('post_num', 'صندوق البريد', 'Post Number'),
('products', 'المنتجات', 'Products'),
('read_more', 'إقرا المزيد', 'Read more'),
('Remember_me', 'تذكرنى', 'Remember me'),
('search', 'بحث...', 'search ...'),
('second', 'ثوانى ', 'Seconds'),
('Select_your_language', 'اختر لغتك', 'Select your language'),
('send_now', 'أرسل الأن', 'Send now'),
('since', 'منذ', 'Since'),
('Social_media', 'وسائل التواصل الاجتماعى', 'Social media'),
('user', 'مستخدم ', 'User'),
('user_name', 'اسم المستخدم ', 'User Name'),
('website', 'الموقع الالكترونى', 'Website');

-- --------------------------------------------------------

--
-- Table structure for table `site_texts`
--

DROP TABLE IF EXISTS `site_texts`;
CREATE TABLE IF NOT EXISTS `site_texts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `site_texts`
--

INSERT INTO `site_texts` (`id`, `name`, `parent_id`, `logo`, `details`, `created_at`, `updated_at`) VALUES
(1, 'نبذة عن المجموعة ', 0, 'cfe8acf7b35aabefd3122d51e482c157.png', 'تعتبر وكالة المتسابق السعودي للدعاية والإعلان والتسويق التي أسسها رجل الاعمال وبطل الشرق الأوسط للراليات / د. محمد بن احمد المالكي في منتصف الثمانينات الميلادية هي اللبنة الأولى لمجموعة المتسابق السعودي القابضة ومن ثم عملنا على إنشاء مؤسسة المتسابق السعودي للإنتاج الفني تلاها تأسيس الفريق السعودي الأول لرياضة السيارات برعاية الرئيس العام لرعاية الشباب صاحب السمو الملكي الأمير / فيصل بن فهد بن عبدالعزيز (رحمه الله) وامتدادا لدعم وتطوير رياضة السيارات انشأنا مركز المتسابق السعودي لصيانة السيارات وتعديلها كما وعملنا على إنشاء أثير الوسطى لتقنية المعلومات و أثير الوسطى لتنظيم المعارض والمؤتمرات والدورات ونعمل في مجموعة المتسابق السعودي القابضة على ابتكار وتنفيذ كل ما يتعلق بهذه المجالات وفق معايير عالمية تواكب رؤية 2030 حيث نعمل وفق نماذج خاصة وعالمية لتحقيق الأهداف الاستراتيجية لشركائنا', '2019-11-12 19:18:19', '2019-11-12 19:18:19'),
(2, 'رؤيتنا ', 0, '2f0e32f9eb2df69785ad842e3db2060f.png', 'نقوم بتقديم وتطوير كل ما يتناسب مع شركائنا ونضيف قيمة متميزة لعملائنا وموظفينا ولكافة أصحاب المصالح .', '2019-11-12 19:20:59', '2019-11-12 19:20:59'),
(3, 'رسالتنا ', 0, '266bbd69f57c2e158392e7decf28e2fa.png', 'نسعى بشكل دائم، من خلال خبرتنا الواسعة، الى تطبيق كل ما هو لازم لتحقيق رؤيتنا، عبر نماذج فريدة، وسنقوم بالوفاء بالتزاماتنا تجاه أصحاب المصالح، الذين نعمل معهم في إطار مجموعة من القيم الجوهرية، التي نتقاسمها مع كل شريك من شركائنا.', '2019-11-12 19:21:21', '2019-11-12 19:21:21'),
(4, 'قيمنا ', 0, 'f33bcdb290e4d272098d8ed1bdc0a459.png', 'ننطلق من فلسفة عميقة ونعتمد تعزيز الجانب القيمي، وذلك وفق تعاليم الشريعة الإسلامية، حيث أننا نؤمن بإعطاء القيم والمبادئ الأولوية في أعمالنا، ان قيمنا التي نتخذها أساساً لأسلوب إدارة أعمالنا، و من بينها', '2019-11-12 19:21:39', '2019-11-12 19:21:39'),
(5, 'أهدافنا ', 0, '3850b174ed5e68020b0635d4a8735fd7.png', '', '2019-11-12 19:21:56', '2019-11-12 19:21:56'),
(6, 'تميزنا فى ', 0, 'f6f85a5cc0a3434cdbcf29cdcc8df5a6.png', 'نؤمن بأن التميز صناعة, لذا نلتزم بتحقيق أعلى معايير الجودة التي تمكّن شركائنا من صناعة النجاح في أعمالهم، ونسعى دائما أن نجمع شركائنا تحت مظلة واحدة من العلاقات التكاملية، التي تحقق استغلال الفرص المتاحة للاستثمار فيها، لبناء منظمات ومنشآت وطنية أكثر إحترافية تكون قادرة على المنافسة في ظل العولمة والمنافسة الإحترافية.\r\n\r\n', '2019-11-12 19:22:33', '2019-11-12 19:22:33'),
(7, 'سرية البيانات', 4, '812304615d43583ac13876cc598d94e7.png', 'جوهر اهتمامنا، وقيمةٌ أساسية يلتزم بها جميع العاملين معنا داخليًا وخارجيًا', '2019-11-12 16:45:33', '2019-11-12 16:45:33'),
(8, 'الأمانة والنزاهة', 4, '660cb66caba0e5a7e85654037bfc7788.png', 'نتولى إدارة اعمالنا بنزاهة وامانة مقدمين عنصري المصداقية والوضوح على أي شيء اخر', '2019-11-12 16:46:17', '2019-11-12 16:46:17'),
(9, 'روح الفريق', 4, '09b9aaed3fef67fab02451e2bd1f6ce0.png', 'تتظافر جهودنا مع فريقنا سواء كانوا عملاء أو موردين أو شركاء أو حتى منافسين لأننا نؤمن بأهمية إقامة علاقات قوية تعتمد على التسامح والتفاهم والعمل لتحقيق المصلحة العامة.', '2019-11-12 16:49:52', '2019-11-12 16:49:52'),
(10, 'الانجاز', 4, 'd8087f0797c60cf8e20359789f10aff3.png', 'نلتزم بالسعي لتحقيق اعلى المعايير الممكنة واجودها عبر أداء متفان ومنظم خلف الكواليس مع جميع أصحاب المصلحة يوازي النتائج التي تظهر على الواقع', '2019-11-12 16:50:11', '2019-11-12 16:50:11'),
(11, 'خدمة العملاء', 4, 'c530f977b6a9870fd9bbe3da21148612.png', 'نوليها لعملائنا بتوفير الدعم الاستشاري والحلول المتخصصة في الوقت المطلوب', '2019-11-12 16:50:36', '2019-11-12 16:50:36'),
(12, 'ان نكسب ثقة شركائنا', 5, '7a7b8e751c3fdd68084c20c55e57022e.png', '', '2019-11-12 16:51:15', '2019-11-12 16:51:15'),
(13, 'تكوين افضل بيئة عمل', 5, '59b82a82939d295fa9b77ba8b7040c3a.png', '', '2019-11-12 16:51:27', '2019-11-12 16:51:27'),
(14, 'استقطاب الكفاءات والمبدعين', 5, 'bd937b19e0fe714e771eb58af8439b9a.png', '', '2019-11-12 16:51:42', '2019-11-12 16:51:42'),
(15, 'ان تكون افعالنا ابلغ من اقوالنا', 5, 'e7c32feb7882b44798587cc8674d971b.png', '', '2019-11-12 16:52:02', '2019-11-12 16:52:02'),
(16, 'ان نكون العلامة التجارية الأولى في تخصصنا', 5, 'ac0b9a58687e1547a270ba3cadac2cc1.png', '', '2019-11-12 16:52:23', '2019-11-12 16:52:23'),
(17, 'منطلقنا رؤية 2030', 6, '09ccbbef0a6fd03d1a532324bca44200.png', 'انطلقنا من فهمنا العميقٍ للصورة الشاملة لتوجهات المملكة والتغيرات الاقتصادية والمبادرات والتشريعات المتسارعة.', '2019-11-12 16:53:00', '2019-11-12 16:53:00'),
(18, 'استشرافُنا المستقبل', 6, 'cb4fd3e48017ef113eb425fe489973fd.png', 'فضلًا عن التزام مجموعة المتسابق السعودي القابضة بإحاطة شركاءها دائما بما يستجد من المتطلبات الحالية، فإننا ننظر إلى ما هو قادم من سياسات وتنظيمات وتغيراتٍ في الاقتصاد ونأخذها بالحسبان في تصميم منتجاتنا.', '2019-11-12 16:53:30', '2019-11-12 16:53:30'),
(19, 'تخصصنا وريادتنا', 6, '94287c5799acf465ea16c0c1df7070ec.png', 'مجموعة المتسابق السعودي القابضة تعد من أوائل الشركات السعودية التي انطلقت بخدمات متعددة ومستشارين متخصصين لتقديم الحلول المبتكرة والنوعية', '2019-11-12 16:54:01', '2019-11-12 16:54:01'),
(20, 'خبرتنا في بيئات عمل متنوعة', 6, 'c6a78043e98bed9af9e8666fe4880e24.png', 'تكمن خبرتنا في الدراية والقدرة على التطبيق والموائمة حسب القطاع والاحتياج المخصص لكل عميل.', '2019-11-12 16:54:35', '2019-11-12 16:54:35');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
CREATE TABLE IF NOT EXISTS `slider` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `name`, `logo`, `details`) VALUES
(1, NULL, '06e887cfe4a86264ccbf3bcccb3b750b.jpg', NULL),
(2, NULL, 'f93e7ed90863c97ded74c845d6d94e65.png', NULL),
(3, NULL, '7489588a84152c10a9d07506099920cf.png', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `team_work`
--

DROP TABLE IF EXISTS `team_work`;
CREATE TABLE IF NOT EXISTS `team_work` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `team_work`
--

INSERT INTO `team_work` (`id`, `name`, `job_title`, `logo`, `details`, `created_at`, `updated_at`) VALUES
(1, 'د. محمد بن احمد المالكي ', 'رئيس مجلس الادارة', '1860b9c147a61887b6f52aedadb7e30d.png', ' مؤسس مجموعة المتسابق السعودي القابضة مؤسس الفريق السعودي الأول لرياضة السيارات حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات عضو مجلس إدارات عدد من الشركات المحلية والعالمية شارك في تأسيس العديد من الشركات المحلية والعالمية مستشارا لعدد من مجالس الإدارات كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات', '2019-11-12 21:43:20', '2019-11-12 21:43:20'),
(2, 'أ. فهد محمد المالكي ', 'نائب رئيس مجلس الادارة', 'dc1237e91897e5b59d729384bf529124.png', ' مؤسس مجموعة المتسابق السعودي القابضة مؤسس الفريق السعودي الأول لرياضة السيارات حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات عضو مجلس إدارات عدد من الشركات المحلية والعالمية شارك في تأسيس العديد من الشركات المحلية والعالمية مستشارا لعدد من مجالس الإدارات كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات', '2019-11-12 21:44:22', '2019-11-12 21:44:22'),
(3, 'أ. احمد باوزير', 'المشرف العام', '9c4de1afd0894c1e22c610af8dac9db1.png', ' مؤسس مجموعة المتسابق السعودي القابضة مؤسس الفريق السعودي الأول لرياضة السيارات حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات عضو مجلس إدارات عدد من الشركات المحلية والعالمية شارك في تأسيس العديد من الشركات المحلية والعالمية مستشارا لعدد من مجالس الإدارات كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات', '2019-11-12 21:45:35', '2019-11-12 21:45:35'),
(4, 'م. احمد الحصيبي ', 'إدارة الاستثمار', 'fb1a548761ab5120410ed370df93a1fc.png', ' مؤسس مجموعة المتسابق السعودي القابضة مؤسس الفريق السعودي الأول لرياضة السيارات حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات عضو مجلس إدارات عدد من الشركات المحلية والعالمية شارك في تأسيس العديد من الشركات المحلية والعالمية مستشارا لعدد من مجالس الإدارات كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات', '2019-11-12 21:46:02', '2019-11-12 21:46:02'),
(5, 'أ. عبدالله الحصيبي', 'الإدارة الفنية', '3c71e89710f8f8aa022a5406490b993f.png', ' مؤسس مجموعة المتسابق السعودي القابضة مؤسس الفريق السعودي الأول لرياضة السيارات حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات عضو مجلس إدارات عدد من الشركات المحلية والعالمية شارك في تأسيس العديد من الشركات المحلية والعالمية مستشارا لعدد من مجالس الإدارات كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات', '2019-11-12 21:46:39', '2019-11-12 21:46:39'),
(6, 'أ. محمود عبدالعال', 'الإدارة المالية', 'e0d4eaefbaeb2a964ae5a656fb0e4a88.png', ' مؤسس مجموعة المتسابق السعودي القابضة مؤسس الفريق السعودي الأول لرياضة السيارات حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات عضو مجلس إدارات عدد من الشركات المحلية والعالمية شارك في تأسيس العديد من الشركات المحلية والعالمية مستشارا لعدد من مجالس الإدارات كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات', '2019-11-12 21:47:12', '2019-11-12 21:47:12'),
(7, 'أ. طاهر بخش ', 'الإدارة الاعلامية', 'a601ca30a765848d5800b774cbca9333.png', ' مؤسس مجموعة المتسابق السعودي القابضة مؤسس الفريق السعودي الأول لرياضة السيارات حاصل على لقب بطل الشرق الأوسط للراليات ثلاثة مرات عضو مجلس إدارات عدد من الشركات المحلية والعالمية شارك في تأسيس العديد من الشركات المحلية والعالمية مستشارا لعدد من مجالس الإدارات كاتب ومهتم في مجال المسؤولية المجتمعية وأيضا لرياضة السيارات', '2019-11-12 21:47:52', '2019-11-12 21:47:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` tinyint(4) NOT NULL DEFAULT '1',
  `is_developer` tinyint(4) NOT NULL DEFAULT '0',
  `is_login` tinyint(4) NOT NULL DEFAULT '0',
  `last_login` int(11) NOT NULL,
  `available` tinyint(4) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_user_username_unique` (`user_username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `password`, `user_username`, `phone`, `image`, `role`, `is_developer`, `is_login`, `last_login`, `available`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'ahmed@ahmed.com', 'adcd7048512e64b48da55b027577886ee5a36350', 'admin', NULL, NULL, 1, 0, 0, 1573718850, 1, NULL, NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `branchs_images`
--
ALTER TABLE `branchs_images`
  ADD CONSTRAINT `branchs_images_main_id_foreign` FOREIGN KEY (`main_id`) REFERENCES `branchs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_publisher_foreign` FOREIGN KEY (`publisher`) REFERENCES `users` (`user_id`) ON DELETE CASCADE;

--
-- Constraints for table `news_images`
--
ALTER TABLE `news_images`
  ADD CONSTRAINT `news_images_main_id_foreign` FOREIGN KEY (`main_id`) REFERENCES `news` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
